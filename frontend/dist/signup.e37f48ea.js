// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

(function (modules, entry, mainEntry, parcelRequireName, globalName) {
  /* eslint-disable no-undef */
  var globalObject =
    typeof globalThis !== 'undefined'
      ? globalThis
      : typeof self !== 'undefined'
      ? self
      : typeof window !== 'undefined'
      ? window
      : typeof global !== 'undefined'
      ? global
      : {};
  /* eslint-enable no-undef */

  // Save the require from previous bundle to this closure if any
  var previousRequire =
    typeof globalObject[parcelRequireName] === 'function' &&
    globalObject[parcelRequireName];

  var cache = previousRequire.cache || {};
  // Do not use `require` to prevent Webpack from trying to bundle this call
  var nodeRequire =
    typeof module !== 'undefined' &&
    typeof module.require === 'function' &&
    module.require.bind(module);

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire =
          typeof globalObject[parcelRequireName] === 'function' &&
          globalObject[parcelRequireName];
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error("Cannot find module '" + name + "'");
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = (cache[name] = new newRequire.Module(name));

      modules[name][0].call(
        module.exports,
        localRequire,
        module,
        module.exports,
        this
      );
    }

    return cache[name].exports;

    function localRequire(x) {
      var res = localRequire.resolve(x);
      return res === false ? {} : newRequire(res);
    }

    function resolve(x) {
      var id = modules[name][1][x];
      return id != null ? id : x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [
      function (require, module) {
        module.exports = exports;
      },
      {},
    ];
  };

  Object.defineProperty(newRequire, 'root', {
    get: function () {
      return globalObject[parcelRequireName];
    },
  });

  globalObject[parcelRequireName] = newRequire;

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (mainEntry) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(mainEntry);

    // CommonJS
    if (typeof exports === 'object' && typeof module !== 'undefined') {
      module.exports = mainExports;

      // RequireJS
    } else if (typeof define === 'function' && define.amd) {
      define(function () {
        return mainExports;
      });

      // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }
})({"kYpTN":[function(require,module,exports) {
var global = arguments[3];
var HMR_HOST = null;
var HMR_PORT = null;
var HMR_SECURE = false;
var HMR_ENV_HASH = "d6ea1d42532a7575";
module.bundle.HMR_BUNDLE_ID = "d113fd8ce37f48ea";
"use strict";
/* global HMR_HOST, HMR_PORT, HMR_ENV_HASH, HMR_SECURE, chrome, browser, __parcel__import__, __parcel__importScripts__, ServiceWorkerGlobalScope */ /*::
import type {
  HMRAsset,
  HMRMessage,
} from '@parcel/reporter-dev-server/src/HMRServer.js';
interface ParcelRequire {
  (string): mixed;
  cache: {|[string]: ParcelModule|};
  hotData: {|[string]: mixed|};
  Module: any;
  parent: ?ParcelRequire;
  isParcelRequire: true;
  modules: {|[string]: [Function, {|[string]: string|}]|};
  HMR_BUNDLE_ID: string;
  root: ParcelRequire;
}
interface ParcelModule {
  hot: {|
    data: mixed,
    accept(cb: (Function) => void): void,
    dispose(cb: (mixed) => void): void,
    // accept(deps: Array<string> | string, cb: (Function) => void): void,
    // decline(): void,
    _acceptCallbacks: Array<(Function) => void>,
    _disposeCallbacks: Array<(mixed) => void>,
  |};
}
interface ExtensionContext {
  runtime: {|
    reload(): void,
    getURL(url: string): string;
    getManifest(): {manifest_version: number, ...};
  |};
}
declare var module: {bundle: ParcelRequire, ...};
declare var HMR_HOST: string;
declare var HMR_PORT: string;
declare var HMR_ENV_HASH: string;
declare var HMR_SECURE: boolean;
declare var chrome: ExtensionContext;
declare var browser: ExtensionContext;
declare var __parcel__import__: (string) => Promise<void>;
declare var __parcel__importScripts__: (string) => Promise<void>;
declare var globalThis: typeof self;
declare var ServiceWorkerGlobalScope: Object;
*/ var OVERLAY_ID = "__parcel__error__overlay__";
var OldModule = module.bundle.Module;
function Module(moduleName) {
    OldModule.call(this, moduleName);
    this.hot = {
        data: module.bundle.hotData[moduleName],
        _acceptCallbacks: [],
        _disposeCallbacks: [],
        accept: function(fn) {
            this._acceptCallbacks.push(fn || function() {});
        },
        dispose: function(fn) {
            this._disposeCallbacks.push(fn);
        }
    };
    module.bundle.hotData[moduleName] = undefined;
}
module.bundle.Module = Module;
module.bundle.hotData = {};
var checkedAssets /*: {|[string]: boolean|} */ , assetsToDispose /*: Array<[ParcelRequire, string]> */ , assetsToAccept /*: Array<[ParcelRequire, string]> */ ;
function getHostname() {
    return HMR_HOST || (location.protocol.indexOf("http") === 0 ? location.hostname : "localhost");
}
function getPort() {
    return HMR_PORT || location.port;
}
// eslint-disable-next-line no-redeclare
var parent = module.bundle.parent;
if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== "undefined") {
    var hostname = getHostname();
    var port = getPort();
    var protocol = HMR_SECURE || location.protocol == "https:" && !/localhost|127.0.0.1|0.0.0.0/.test(hostname) ? "wss" : "ws";
    var ws;
    try {
        ws = new WebSocket(protocol + "://" + hostname + (port ? ":" + port : "") + "/");
    } catch (err) {
        if (err.message) console.error(err.message);
        ws = {};
    }
    // Web extension context
    var extCtx = typeof browser === "undefined" ? typeof chrome === "undefined" ? null : chrome : browser;
    // Safari doesn't support sourceURL in error stacks.
    // eval may also be disabled via CSP, so do a quick check.
    var supportsSourceURL = false;
    try {
        (0, eval)('throw new Error("test"); //# sourceURL=test.js');
    } catch (err) {
        supportsSourceURL = err.stack.includes("test.js");
    }
    // $FlowFixMe
    ws.onmessage = async function(event /*: {data: string, ...} */ ) {
        checkedAssets = {} /*: {|[string]: boolean|} */ ;
        assetsToAccept = [];
        assetsToDispose = [];
        var data /*: HMRMessage */  = JSON.parse(event.data);
        if (data.type === "update") {
            // Remove error overlay if there is one
            if (typeof document !== "undefined") removeErrorOverlay();
            let assets = data.assets.filter((asset)=>asset.envHash === HMR_ENV_HASH);
            // Handle HMR Update
            let handled = assets.every((asset)=>{
                return asset.type === "css" || asset.type === "js" && hmrAcceptCheck(module.bundle.root, asset.id, asset.depsByBundle);
            });
            if (handled) {
                console.clear();
                // Dispatch custom event so other runtimes (e.g React Refresh) are aware.
                if (typeof window !== "undefined" && typeof CustomEvent !== "undefined") window.dispatchEvent(new CustomEvent("parcelhmraccept"));
                await hmrApplyUpdates(assets);
                // Dispose all old assets.
                let processedAssets = {} /*: {|[string]: boolean|} */ ;
                for(let i = 0; i < assetsToDispose.length; i++){
                    let id = assetsToDispose[i][1];
                    if (!processedAssets[id]) {
                        hmrDispose(assetsToDispose[i][0], id);
                        processedAssets[id] = true;
                    }
                }
                // Run accept callbacks. This will also re-execute other disposed assets in topological order.
                processedAssets = {};
                for(let i = 0; i < assetsToAccept.length; i++){
                    let id = assetsToAccept[i][1];
                    if (!processedAssets[id]) {
                        hmrAccept(assetsToAccept[i][0], id);
                        processedAssets[id] = true;
                    }
                }
            } else fullReload();
        }
        if (data.type === "error") {
            // Log parcel errors to console
            for (let ansiDiagnostic of data.diagnostics.ansi){
                let stack = ansiDiagnostic.codeframe ? ansiDiagnostic.codeframe : ansiDiagnostic.stack;
                console.error("\uD83D\uDEA8 [parcel]: " + ansiDiagnostic.message + "\n" + stack + "\n\n" + ansiDiagnostic.hints.join("\n"));
            }
            if (typeof document !== "undefined") {
                // Render the fancy html overlay
                removeErrorOverlay();
                var overlay = createErrorOverlay(data.diagnostics.html);
                // $FlowFixMe
                document.body.appendChild(overlay);
            }
        }
    };
    ws.onerror = function(e) {
        if (e.message) console.error(e.message);
    };
    ws.onclose = function() {
        console.warn("[parcel] \uD83D\uDEA8 Connection to the HMR server was lost");
    };
}
function removeErrorOverlay() {
    var overlay = document.getElementById(OVERLAY_ID);
    if (overlay) {
        overlay.remove();
        console.log("[parcel] \u2728 Error resolved");
    }
}
function createErrorOverlay(diagnostics) {
    var overlay = document.createElement("div");
    overlay.id = OVERLAY_ID;
    let errorHTML = '<div style="background: black; opacity: 0.85; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; font-family: Menlo, Consolas, monospace; z-index: 9999;">';
    for (let diagnostic of diagnostics){
        let stack = diagnostic.frames.length ? diagnostic.frames.reduce((p, frame)=>{
            return `${p}
<a href="/__parcel_launch_editor?file=${encodeURIComponent(frame.location)}" style="text-decoration: underline; color: #888" onclick="fetch(this.href); return false">${frame.location}</a>
${frame.code}`;
        }, "") : diagnostic.stack;
        errorHTML += `
      <div>
        <div style="font-size: 18px; font-weight: bold; margin-top: 20px;">
          \u{1F6A8} ${diagnostic.message}
        </div>
        <pre>${stack}</pre>
        <div>
          ${diagnostic.hints.map((hint)=>"<div>\uD83D\uDCA1 " + hint + "</div>").join("")}
        </div>
        ${diagnostic.documentation ? `<div>\u{1F4DD} <a style="color: violet" href="${diagnostic.documentation}" target="_blank">Learn more</a></div>` : ""}
      </div>
    `;
    }
    errorHTML += "</div>";
    overlay.innerHTML = errorHTML;
    return overlay;
}
function fullReload() {
    if ("reload" in location) location.reload();
    else if (extCtx && extCtx.runtime && extCtx.runtime.reload) extCtx.runtime.reload();
}
function getParents(bundle, id) /*: Array<[ParcelRequire, string]> */ {
    var modules = bundle.modules;
    if (!modules) return [];
    var parents = [];
    var k, d, dep;
    for(k in modules)for(d in modules[k][1]){
        dep = modules[k][1][d];
        if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) parents.push([
            bundle,
            k
        ]);
    }
    if (bundle.parent) parents = parents.concat(getParents(bundle.parent, id));
    return parents;
}
function updateLink(link) {
    var href = link.getAttribute("href");
    if (!href) return;
    var newLink = link.cloneNode();
    newLink.onload = function() {
        if (link.parentNode !== null) // $FlowFixMe
        link.parentNode.removeChild(link);
    };
    newLink.setAttribute("href", // $FlowFixMe
    href.split("?")[0] + "?" + Date.now());
    // $FlowFixMe
    link.parentNode.insertBefore(newLink, link.nextSibling);
}
var cssTimeout = null;
function reloadCSS() {
    if (cssTimeout) return;
    cssTimeout = setTimeout(function() {
        var links = document.querySelectorAll('link[rel="stylesheet"]');
        for(var i = 0; i < links.length; i++){
            // $FlowFixMe[incompatible-type]
            var href /*: string */  = links[i].getAttribute("href");
            var hostname = getHostname();
            var servedFromHMRServer = hostname === "localhost" ? new RegExp("^(https?:\\/\\/(0.0.0.0|127.0.0.1)|localhost):" + getPort()).test(href) : href.indexOf(hostname + ":" + getPort());
            var absolute = /^https?:\/\//i.test(href) && href.indexOf(location.origin) !== 0 && !servedFromHMRServer;
            if (!absolute) updateLink(links[i]);
        }
        cssTimeout = null;
    }, 50);
}
function hmrDownload(asset) {
    if (asset.type === "js") {
        if (typeof document !== "undefined") {
            let script = document.createElement("script");
            script.src = asset.url + "?t=" + Date.now();
            if (asset.outputFormat === "esmodule") script.type = "module";
            return new Promise((resolve, reject)=>{
                var _document$head;
                script.onload = ()=>resolve(script);
                script.onerror = reject;
                (_document$head = document.head) === null || _document$head === void 0 || _document$head.appendChild(script);
            });
        } else if (typeof importScripts === "function") {
            // Worker scripts
            if (asset.outputFormat === "esmodule") return import(asset.url + "?t=" + Date.now());
            else return new Promise((resolve, reject)=>{
                try {
                    importScripts(asset.url + "?t=" + Date.now());
                    resolve();
                } catch (err) {
                    reject(err);
                }
            });
        }
    }
}
async function hmrApplyUpdates(assets) {
    global.parcelHotUpdate = Object.create(null);
    let scriptsToRemove;
    try {
        // If sourceURL comments aren't supported in eval, we need to load
        // the update from the dev server over HTTP so that stack traces
        // are correct in errors/logs. This is much slower than eval, so
        // we only do it if needed (currently just Safari).
        // https://bugs.webkit.org/show_bug.cgi?id=137297
        // This path is also taken if a CSP disallows eval.
        if (!supportsSourceURL) {
            let promises = assets.map((asset)=>{
                var _hmrDownload;
                return (_hmrDownload = hmrDownload(asset)) === null || _hmrDownload === void 0 ? void 0 : _hmrDownload.catch((err)=>{
                    // Web extension fix
                    if (extCtx && extCtx.runtime && extCtx.runtime.getManifest().manifest_version == 3 && typeof ServiceWorkerGlobalScope != "undefined" && global instanceof ServiceWorkerGlobalScope) {
                        extCtx.runtime.reload();
                        return;
                    }
                    throw err;
                });
            });
            scriptsToRemove = await Promise.all(promises);
        }
        assets.forEach(function(asset) {
            hmrApply(module.bundle.root, asset);
        });
    } finally{
        delete global.parcelHotUpdate;
        if (scriptsToRemove) scriptsToRemove.forEach((script)=>{
            if (script) {
                var _document$head2;
                (_document$head2 = document.head) === null || _document$head2 === void 0 || _document$head2.removeChild(script);
            }
        });
    }
}
function hmrApply(bundle /*: ParcelRequire */ , asset /*:  HMRAsset */ ) {
    var modules = bundle.modules;
    if (!modules) return;
    if (asset.type === "css") reloadCSS();
    else if (asset.type === "js") {
        let deps = asset.depsByBundle[bundle.HMR_BUNDLE_ID];
        if (deps) {
            if (modules[asset.id]) {
                // Remove dependencies that are removed and will become orphaned.
                // This is necessary so that if the asset is added back again, the cache is gone, and we prevent a full page reload.
                let oldDeps = modules[asset.id][1];
                for(let dep in oldDeps)if (!deps[dep] || deps[dep] !== oldDeps[dep]) {
                    let id = oldDeps[dep];
                    let parents = getParents(module.bundle.root, id);
                    if (parents.length === 1) hmrDelete(module.bundle.root, id);
                }
            }
            if (supportsSourceURL) // Global eval. We would use `new Function` here but browser
            // support for source maps is better with eval.
            (0, eval)(asset.output);
            // $FlowFixMe
            let fn = global.parcelHotUpdate[asset.id];
            modules[asset.id] = [
                fn,
                deps
            ];
        } else if (bundle.parent) hmrApply(bundle.parent, asset);
    }
}
function hmrDelete(bundle, id) {
    let modules = bundle.modules;
    if (!modules) return;
    if (modules[id]) {
        // Collect dependencies that will become orphaned when this module is deleted.
        let deps = modules[id][1];
        let orphans = [];
        for(let dep in deps){
            let parents = getParents(module.bundle.root, deps[dep]);
            if (parents.length === 1) orphans.push(deps[dep]);
        }
        // Delete the module. This must be done before deleting dependencies in case of circular dependencies.
        delete modules[id];
        delete bundle.cache[id];
        // Now delete the orphans.
        orphans.forEach((id)=>{
            hmrDelete(module.bundle.root, id);
        });
    } else if (bundle.parent) hmrDelete(bundle.parent, id);
}
function hmrAcceptCheck(bundle /*: ParcelRequire */ , id /*: string */ , depsByBundle /*: ?{ [string]: { [string]: string } }*/ ) {
    if (hmrAcceptCheckOne(bundle, id, depsByBundle)) return true;
    // Traverse parents breadth first. All possible ancestries must accept the HMR update, or we'll reload.
    let parents = getParents(module.bundle.root, id);
    let accepted = false;
    while(parents.length > 0){
        let v = parents.shift();
        let a = hmrAcceptCheckOne(v[0], v[1], null);
        if (a) // If this parent accepts, stop traversing upward, but still consider siblings.
        accepted = true;
        else {
            // Otherwise, queue the parents in the next level upward.
            let p = getParents(module.bundle.root, v[1]);
            if (p.length === 0) {
                // If there are no parents, then we've reached an entry without accepting. Reload.
                accepted = false;
                break;
            }
            parents.push(...p);
        }
    }
    return accepted;
}
function hmrAcceptCheckOne(bundle /*: ParcelRequire */ , id /*: string */ , depsByBundle /*: ?{ [string]: { [string]: string } }*/ ) {
    var modules = bundle.modules;
    if (!modules) return;
    if (depsByBundle && !depsByBundle[bundle.HMR_BUNDLE_ID]) {
        // If we reached the root bundle without finding where the asset should go,
        // there's nothing to do. Mark as "accepted" so we don't reload the page.
        if (!bundle.parent) return true;
        return hmrAcceptCheck(bundle.parent, id, depsByBundle);
    }
    if (checkedAssets[id]) return true;
    checkedAssets[id] = true;
    var cached = bundle.cache[id];
    assetsToDispose.push([
        bundle,
        id
    ]);
    if (!cached || cached.hot && cached.hot._acceptCallbacks.length) {
        assetsToAccept.push([
            bundle,
            id
        ]);
        return true;
    }
}
function hmrDispose(bundle /*: ParcelRequire */ , id /*: string */ ) {
    var cached = bundle.cache[id];
    bundle.hotData[id] = {};
    if (cached && cached.hot) cached.hot.data = bundle.hotData[id];
    if (cached && cached.hot && cached.hot._disposeCallbacks.length) cached.hot._disposeCallbacks.forEach(function(cb) {
        cb(bundle.hotData[id]);
    });
    delete bundle.cache[id];
}
function hmrAccept(bundle /*: ParcelRequire */ , id /*: string */ ) {
    // Execute the module.
    bundle(id);
    // Run the accept callbacks in the new version of the module.
    var cached = bundle.cache[id];
    if (cached && cached.hot && cached.hot._acceptCallbacks.length) cached.hot._acceptCallbacks.forEach(function(cb) {
        var assetsToAlsoAccept = cb(function() {
            return getParents(module.bundle.root, id);
        });
        if (assetsToAlsoAccept && assetsToAccept.length) {
            assetsToAlsoAccept.forEach(function(a) {
                hmrDispose(a[0], a[1]);
            });
            // $FlowFixMe[method-unbinding]
            assetsToAccept.push.apply(assetsToAccept, assetsToAlsoAccept);
        }
    });
}

},{}],"aenu9":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
var _signupViewJs = require("./views/signupView.js");
var _signupViewJsDefault = parcelHelpers.interopDefault(_signupViewJs);
var _loginViewJs = require("./views/loginView.js");
var _loginViewJsDefault = parcelHelpers.interopDefault(_loginViewJs);
var _errorViewJs = require("./views/errorView.js");
var _errorViewJsDefault = parcelHelpers.interopDefault(_errorViewJs);
var _dashboardViewJs = require("./views/dashboardView.js");
var _dashboardViewJsDefault = parcelHelpers.interopDefault(_dashboardViewJs);
var _addProjectViewJs = require("./views/addProjectView.js");
var _addProjectViewJsDefault = parcelHelpers.interopDefault(_addProjectViewJs);
var _projectViewJs = require("./views/projectView.js");
var _projectViewJsDefault = parcelHelpers.interopDefault(_projectViewJs);
var _projectLocationViewJs = require("./views/projectLocationView.js");
var _projectLocationViewJsDefault = parcelHelpers.interopDefault(_projectLocationViewJs);
var _addTaskViewJs = require("./views/addTaskView.js");
var _addTaskViewJsDefault = parcelHelpers.interopDefault(_addTaskViewJs);
var _cardNavViewJs = require("./views/cardNavView.js");
var _cardNavViewJsDefault = parcelHelpers.interopDefault(_cardNavViewJs);
var _cardViewJs = require("./views/cardView.js");
var _cardViewJsDefault = parcelHelpers.interopDefault(_cardViewJs);
var _logoutViewJs = require("./views/logoutView.js");
var _logoutViewJsDefault = parcelHelpers.interopDefault(_logoutViewJs);
var _modelJs = require("./model.js");
if (module.hot) module.hot.accept();
// Register - login - store users in Strapi - enter dashboard
const form = document.querySelector(".form");
const toDashboard = function() {
    setTimeout(()=>{
        if (localStorage.getItem("jwt") !== null) (0, _dashboardViewJsDefault.default).goToDashboard();
    }, 200);
};
const init = async function() {
    if (window.location.href === "http://localhost:1234/login.html") {
        await (0, _loginViewJsDefault.default).addUser();
        const person = (0, _loginViewJsDefault.default).user;
        _modelJs.state.push(person);
        const err = await _modelJs.findUser(_modelJs.state[_modelJs.state.length - 1]);
        (0, _errorViewJsDefault.default).renderError(err);
        toDashboard();
    } else if (window.location.href === "http://localhost:1234/signup.html" || window.location.href === "http://localhost:1234") {
        await (0, _signupViewJsDefault.default).addUser();
        const person = (0, _signupViewJsDefault.default).user;
        _modelJs.state.push(person);
        const err = await _modelJs.uploadUser(_modelJs.state[_modelJs.state.length - 1]);
        (0, _errorViewJsDefault.default).renderError(err);
        toDashboard();
    }
};
form.addEventListener("submit", function(e) {
    e.preventDefault();
    init();
});
// logout
const logout = function() {
    if (window.location.href === "http://localhost:1234/dashboard.html") (0, _logoutViewJsDefault.default).logout();
};
logout();
// Redirection
const redirection = function() {
    if (localStorage.getItem("jwt") !== null) {
        if (window.location.href === "http://localhost:1234/dashboard.html") return;
        (0, _dashboardViewJsDefault.default).goToDashboard();
    }
    if (window.location.href === "http://localhost:1234/dashboard.html" && localStorage.getItem("jwt") === null || window.location.href === "http://localhost:1234/dashboard.html" && localStorage.getItem("jwt") === "undefined") (0, _signupViewJsDefault.default).toSignup();
};
redirection();
// Add Project and render it
const mainLogic = async function() {
    if (window.location.href === "http://localhost:1234/dashboard.html") {
        // get the object of the current user
        const me = JSON.parse(localStorage.getItem("data"));
        // Get users from Strapi
        const state = await _modelJs.findUsers();
        _modelJs.state.push(state);
        // Register project in Strapi using a modal then link it to the current user
        (0, _addProjectViewJsDefault.default).getProjectInput(me);
        // Render projects : To Do
        const myProj = await _modelJs.findMyData(me, "projects");
        console.log(myProj);
        myProj.map((project)=>(0, _projectViewJsDefault.default).renderProject(project.attributes, myProj));
        (0, _projectLocationViewJsDefault.default).renderProjectLocation();
        // Add Task to Strapi
        const addTask = async function(myProject) {
            console.log(myProject);
            (0, _addTaskViewJsDefault.default).addTask(myProject);
        };
        await addTask(myProj);
        // To Do: Render list & board View with drag & drop
        (0, _cardNavViewJsDefault.default).renderCardNav();
        window.addEventListener("click", function() {
            (0, _cardViewJsDefault.default).renderCard(myProj);
        });
    }
};
mainLogic();

},{"./views/signupView.js":"2Qbus","./views/loginView.js":"1Ao0H","./views/errorView.js":"eLcq5","./views/dashboardView.js":"ie8VL","./views/addProjectView.js":"c5YkZ","./views/projectView.js":"buQla","./views/projectLocationView.js":"kthUK","./views/addTaskView.js":"dKG6e","./views/cardNavView.js":"8wuHO","./views/cardView.js":"gEib3","./views/logoutView.js":"hkjus","./model.js":"Y4A21","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"2Qbus":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
class SignupView {
    _form = document.querySelector(".form--signup");
    _fullName = document.querySelector(".input__name");
    _email = document.querySelector(".input__email");
    _password = document.querySelector(".input__password");
    _phone = document.querySelector(".input__phone");
    user = {};
    toSignup() {
        console.log(1);
        window.location.replace("http://localhost:1234/signup.html");
        localStorage.clear();
    }
    async addUser() {
        const handler = function(e) {
            e.preventDefault();
            this.user = {
                username: this._fullName.value,
                email: this._email.value,
                password: this._password.value,
                phone: this._phone.value
            };
        };
        this._form.addEventListener("submit", handler.bind(this));
        const { data } = await new Promise((handler)=>{
            this._form.addEventListener("submit", handler.bind(this));
        });
        return data;
    }
}
exports.default = new SignupView();

},{"@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"gkKU3":[function(require,module,exports) {
exports.interopDefault = function(a) {
    return a && a.__esModule ? a : {
        default: a
    };
};
exports.defineInteropFlag = function(a) {
    Object.defineProperty(a, "__esModule", {
        value: true
    });
};
exports.exportAll = function(source, dest) {
    Object.keys(source).forEach(function(key) {
        if (key === "default" || key === "__esModule" || Object.prototype.hasOwnProperty.call(dest, key)) return;
        Object.defineProperty(dest, key, {
            enumerable: true,
            get: function() {
                return source[key];
            }
        });
    });
    return dest;
};
exports.export = function(dest, destName, get) {
    Object.defineProperty(dest, destName, {
        enumerable: true,
        get: get
    });
};

},{}],"1Ao0H":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
class LoginView {
    _form = document.querySelector(".form--login");
    _email = document.querySelector(".input__email");
    _password = document.querySelector(".input__password");
    user = {};
    toLogin() {
        window.location.replace("http://localhost:1234/login.html");
        localStorage.clear();
    }
    async addUser() {
        const handler = function(e) {
            e.preventDefault();
            this.user = {
                identifier: this._email.value,
                password: this._password.value
            };
        };
        this._form.addEventListener("submit", handler.bind(this));
        const { data } = await new Promise((handler)=>{
            this._form.addEventListener("submit", handler.bind(this));
        });
        return data;
    }
}
exports.default = new LoginView();

},{"@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"eLcq5":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
class ErrorView {
    parent = document.querySelector(".btn");
    renderError(err) {
        const html = `
      <div class="error">
        <p>${err}</p>
      </div>
    `;
        console.log(document.querySelector(".error"));
        if (document.querySelector(".error") !== null) return;
        setTimeout(()=>{
            this.parent.insertAdjacentHTML("beforebegin", html);
        }, 300);
        setTimeout(()=>{
            document.querySelector(".error").remove();
        }, 3000);
    }
}
exports.default = new ErrorView();

},{"@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"ie8VL":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
class DashboardView {
    goToDashboard() {
        window.location.replace("http://localhost:1234/dashboard.html");
    }
}
exports.default = new DashboardView();

},{"@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"c5YkZ":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
var _configJs = require("./../config.js");
var _helpersJs = require("./../helpers.js");
var _modelJs = require("./../model.js");
class AddProjectView {
    btn = document.querySelector(".project__btn");
    async getProjectInput(currentUser) {
        const html = `
      <div class="add-project-window">
        <div class="modal">
          <h1 class="modal__project__heading">Add a new Project</h1>
          <form class="modal__project__form">
            <div>
              <p class="modal__project__label">Project's name </p>
              <input class="modal__project__input project__input--name" type="text" placeholder="Project's name">
            </div>
            <div>
              <p class="modal__project__label">Project's image url </p>
              <input class="modal__project__input project__input--img" type="text" placeholder="Project's image">
            </div>
            <button class="pointer btn--add-project">Submit</button>
          </form>
        </div>
        <div class="overlay"></div>
      </div>
    `;
        const btn = this.btn;
        const registerProject = async function() {
            btn.addEventListener("click", function(e) {
                e.preventDefault();
                this.closest("body").insertAdjacentHTML("afterbegin", html);
                this.closest("body").querySelector("form").addEventListener("click", function(e) {
                    e.preventDefault();
                    const name = this.closest(".modal").querySelector(".project__input--name").value;
                    if (name === "") return;
                    const imgURL = this.closest(".modal").querySelector(".project__input--img").value;
                    if (imgURL === "") return;
                    const project = {
                        data: {
                            name: name,
                            img: imgURL
                        }
                    };
                    const uploadProject = async function(project) {
                        try {
                            const filteredProjects = await _modelJs.findMyData(currentUser, "projects");
                            const projectEl = await (0, _helpersJs.AJAX)(`${(0, _configJs.API_URL)}/projects`, project);
                            // I added filtered Projects
                            const projects = {
                                projects: [
                                    ...filteredProjects,
                                    projectEl.data
                                ]
                            };
                            console.log(projects);
                            await _modelJs.updateRelations(`${(0, _configJs.API_URL)}/users/${currentUser.id}`, projects);
                            // Remove window
                            document.querySelector(".add-project-window").remove();
                            window.location.reload();
                        } catch (err) {
                            return `${err.message}`;
                        }
                    };
                    uploadProject(project);
                });
                this.closest("body").querySelector(".overlay").addEventListener("click", function() {
                    this.closest(".add-project-window").remove();
                });
            });
        };
        registerProject();
    }
}
exports.default = new AddProjectView();

},{"./../config.js":"k5Hzs","./../helpers.js":"hGI1E","./../model.js":"Y4A21","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"k5Hzs":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
parcelHelpers.export(exports, "API_URL", ()=>API_URL);
parcelHelpers.export(exports, "TIMEOUT_SEC", ()=>TIMEOUT_SEC);
const API_URL = "http://localhost:1337/api";
const TIMEOUT_SEC = 10;

},{"@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"hGI1E":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
parcelHelpers.export(exports, "timeout", ()=>timeout);
parcelHelpers.export(exports, "AJAXaccess", ()=>AJAXaccess);
parcelHelpers.export(exports, "AJAX", ()=>AJAX);
var _config = require("./config");
var _model = require("./model");
const timeout = function(s) {
    return new Promise(function(_, reject) {
        setTimeout(function() {
            reject(new Error(`Request took too long! Timeout after ${s} second`));
        }, s * 1000);
    });
};
const AJAXaccess = async function(url, uploadData) {
    try {
        const fetchPro = uploadData ? fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(uploadData)
        }) : fetch(url);
        const res = await Promise.race([
            fetchPro,
            timeout((0, _config.TIMEOUT_SEC))
        ]);
        console.log(res);
        const data = await res.json();
        console.log(data);
        if (data.data !== null) {
            localStorage.setItem("data", JSON.stringify(data.user));
            localStorage.setItem("jwt", data.jwt);
            localStorage.setItem("user", data.user.id);
            console.log(data);
            _model.state.push(data.user);
        }
        if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);
        return data;
    } catch (err) {
        throw err;
    }
};
const AJAX = async function(url, uploadData) {
    try {
        console.log(uploadData, url);
        const fetchPro = uploadData ? fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(uploadData)
        }) : fetch(url);
        const res = await Promise.race([
            fetchPro,
            timeout((0, _config.TIMEOUT_SEC))
        ]);
        console.log(res);
        const data = await res.json();
        console.log(data);
        if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);
        return data;
    } catch (err) {
        throw err;
    }
};

},{"./config":"k5Hzs","./model":"Y4A21","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"Y4A21":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
parcelHelpers.export(exports, "state", ()=>state);
parcelHelpers.export(exports, "uploadUser", ()=>uploadUser);
parcelHelpers.export(exports, "findUser", ()=>findUser);
parcelHelpers.export(exports, "findUsers", ()=>findUsers);
parcelHelpers.export(exports, "updateRelations", ()=>updateRelations);
parcelHelpers.export(exports, "findMyData", ()=>findMyData);
var _configJs = require("./config.js");
var _helpersJs = require("./helpers.js");
const state = [];
const uploadUser = async function(data) {
    try {
        await (0, _helpersJs.AJAXaccess)(`${(0, _configJs.API_URL)}/auth/local/register`, data);
        state = (0, _helpersJs.AJAX)(`${(0, _configJs.API_URL)}/users`);
    } catch (err) {
        return `${err.message}`;
    }
};
const findUser = async function(data) {
    try {
        await (0, _helpersJs.AJAXaccess)(`${(0, _configJs.API_URL)}/auth/local`, data);
    } catch (err) {
        return `${err.message}`;
    }
};
const findUsers = async function() {
    try {
        const fetchPro = fetch(`${(0, _configJs.API_URL)}/users`);
        const res = await Promise.race([
            fetchPro,
            (0, _helpersJs.timeout)((0, _configJs.TIMEOUT_SEC))
        ]);
        const data = await res.json();
        if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);
        return data;
    } catch (err) {
        throw err;
    }
};
const updateRelations = async function(url, uploadData) {
    try {
        const fetchPro = fetch(url, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(uploadData)
        });
        const res = await Promise.race([
            fetchPro,
            (0, _helpersJs.timeout)((0, _configJs.TIMEOUT_SEC))
        ]);
        const data = await res.json();
        if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);
        return data;
    } catch (err) {
        throw err;
    }
};
const findMyData = async function(parent, dataType) {
    try {
        const fetchPro = fetch(`${(0, _configJs.API_URL)}/${dataType}?populate=*`);
        const res = await Promise.race([
            fetchPro,
            (0, _helpersJs.timeout)((0, _configJs.TIMEOUT_SEC))
        ]);
        const data = await res.json();
        if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);
        const allData = data.data;
        let filteredData;
        switch(dataType){
            case "projects":
                filteredData = allData.reduce((acc, cur)=>{
                    cur.attributes.users.data[0]?.id === parent.id ? acc.push(cur) : console.log(acc);
                    console.log(acc);
                    return acc;
                }, []);
                break;
            case "tasks":
                filteredData = allData.reduce((acc, cur)=>{
                    cur.attributes.statuses.data[0]?.id === parent.id ? acc.push(cur) : console.log(acc);
                    return acc;
                }, []);
                break;
            case "statuses":
                filteredData = allData.reduce((acc, cur)=>{
                    console.log(cur);
                    cur.attributes.projects.data[0]?.id === parent.id ? acc.push(cur) : console.log(acc);
                    return acc;
                }, []);
                break;
        }
        return filteredData;
    } catch (err) {
        throw err;
    }
};

},{"./config.js":"k5Hzs","./helpers.js":"hGI1E","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"buQla":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
var _addTaskView = require("./addTaskView");
var _addTaskViewDefault = parcelHelpers.interopDefault(_addTaskView);
class ProjectView {
    parent = document.querySelector(".navigation-bar__projects");
    renderProject(project, myProject) {
        const html = `
      <div class="navigation-bar__project">
        <div class="project__title__el pointer">
          <div>
            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="16" viewBox="0 0 15 16" fill="none">
              <path d="M5.62507 11.675V4.69379C5.62435 4.56965 5.66061 4.44811 5.72924 4.34466C5.79787 4.24121 5.89574 4.16054 6.0104 4.11293C6.12505 4.06533 6.25128 4.05295 6.373 4.07737C6.49472 4.10178 6.6064 4.1619 6.69382 4.25004L10.1813 7.74379C10.2977 7.86089 10.3631 8.0193 10.3631 8.18441C10.3631 8.34953 10.2977 8.50794 10.1813 8.62504L6.69382 12.1188C6.6064 12.2069 6.49472 12.267 6.373 12.2915C6.25128 12.3159 6.12505 12.3035 6.0104 12.2559C5.89574 12.2083 5.79787 12.1276 5.72924 12.0242C5.66061 11.9207 5.62435 11.7992 5.62507 11.675Z" fill="#98A2B3"/>
            </svg>
          </div>
          <img src="${project.img}" class="project__img" alt="project icon" />
          <p data-name="${project.name}" class="projectName">${project.name}</p>
        </div>
        <div class="project__func__el">
          <div class="pointer">
            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10" fill="none">
              <g clip-path="url(#clip0_103_2804)">
                <path d="M5.00002 1.66667C5.46026 1.66667 5.83335 1.29357 5.83335 0.833333C5.83335 0.373096 5.46026 0 5.00002 0C4.53978 0 4.16669 0.373096 4.16669 0.833333C4.16669 1.29357 4.53978 1.66667 5.00002 1.66667Z" fill="#666666"/>
                <path d="M5.00002 5.8335C5.46026 5.8335 5.83335 5.46041 5.83335 5.00017C5.83335 4.53993 5.46026 4.16684 5.00002 4.16684C4.53978 4.16684 4.16669 4.53993 4.16669 5.00017C4.16669 5.46041 4.53978 5.8335 5.00002 5.8335Z" fill="#666666"/>
                <path d="M5.00002 9.99984C5.46026 9.99984 5.83335 9.62674 5.83335 9.1665C5.83335 8.70626 5.46026 8.33316 5.00002 8.33316C4.53978 8.33316 4.16669 8.70626 4.16669 9.1665C4.16669 9.62674 4.53978 9.99984 5.00002 9.99984Z" fill="#666666"/>
              </g>
              <defs>
                <clipPath id="clip0_103_2804">
                  <rect width="10" height="10" fill="white"/>
                </clipPath>
              </defs>
            </svg>
          </div>
          <div class="pointer">
            <p class="project__func__el__p">+</p>
          </div>
        </div>
      </div>
    `;
        let allDisplayedProjects = new Set();
        document.querySelectorAll(".projectName").forEach((pr)=>{
            allDisplayedProjects.add(pr.dataset.name);
            return pr.dataset.name;
        });
        if (!allDisplayedProjects.has(project.name)) this.parent.insertAdjacentHTML("beforeend", html);
        // this.parent.querySelectorAll('.project__func__el__p').forEach(el => {
        //   el.addEventListener('click', function () {
        //     const firstBtnClicked = true;
        //     addTaskView.addTask(myProject, firstBtnClicked);
        //   });
        // });
        this.parent.addEventListener("click", function(e) {
            this.querySelectorAll(".navigation-bar__project").forEach((el)=>{
                el.classList.remove("navigation-bar__project--selected");
            });
            e.target.closest(".navigation-bar__project")?.classList.add("navigation-bar__project--selected");
        });
    }
}
exports.default = new ProjectView();

},{"./addTaskView":"dKG6e","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"dKG6e":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
var _configJs = require("./../config.js");
var _helpersJs = require("./../helpers.js");
var _modelJs = require("./../model.js");
class AddTaskView {
    btnAddTask = document.querySelector(".btn--newtask");
    async saveTask(title, description, priority, attachement, myStatus) {
        try {
            const fileReader = new FileReader();
            let attachementEl;
            if (attachement) {
                fileReader.readAsDataURL(attachement);
                fileReader.addEventListener("load", function() {
                    attachementEl = this.result;
                });
            } else attachementEl = "";
            // Task is added to project so I have to add it to correspond it status instead
            const filtredTasks = await _modelJs.findMyData(myStatus, "tasks");
            const task = {
                data: {
                    title: title,
                    description: description,
                    priority: priority,
                    attachement: attachementEl
                }
            };
            const currentTask = await (0, _helpersJs.AJAX)(`${(0, _configJs.API_URL)}/tasks`, task);
            const tasks = {
                data: {
                    tasks: [
                        ...filtredTasks,
                        currentTask.data
                    ]
                }
            };
            await _modelJs.updateRelations(`${(0, _configJs.API_URL)}/statuses/${myStatus.id}`, tasks);
            document.querySelector("body").click();
        } catch (err) {
            return `${err.message}`;
        }
    }
    addTask(myProj, btnAddTaskHandler) {
        let proj;
        let projectStatuses;
        let saveTask = this.saveTask;
        let currentStatus;
        console.log(document.querySelectorAll(".card__view__add-task"));
        document.querySelectorAll(".card__view__add-task").forEach((pr)=>{
            console.log(pr);
            pr.addEventListener("click", function() {
                console.log(pr);
                currentStatus = this.closest(".card__list__view__block__header__status").querySelector(".card__list__view__block__header__status__text").textContent.trim();
                setTimeout(async ()=>{
                    proj = myProj.filter((proj)=>proj.attributes.name === document.querySelector(".navigation-bar__project--selected")?.querySelector(".project__title__el")?.querySelector("p").textContent);
                    projectStatuses = await _modelJs.findMyData(proj[0], "statuses");
                // const specificStatus = projectStatuses.filter(
                //   state => state.attributes.status === currentStatus
                // )[0];
                // console.log(specificStatus);
                }, 300);
                setTimeout(()=>{
                    let statusColor = "#FFA948";
                    let priorityColor = "#FFB700";
                    let priorityText = "High";
                    console.log(this.closest(".card__list__view__block__header__status").querySelector(".card__list__view__block__header__status__text").textContent.trim());
                    const html = `
            <div class="add-task-window">
              <div class="modal task--modal">
                <div class="task__header">
                  <p class="task__location">Project</p>
                  <div class="task__close">
                    <span class="task__date">Created 27/11/23</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27" fill="none">
                      <path d="M13.0975 13.2023H13.8964M13.0975 14.0012H13.8964M5.6412 13.2023H6.44009M5.6412 14.0012H6.44009M20.5538 13.2023H21.3527M20.5538 14.0012H21.3527M14.5624 13.6018C14.5624 14.1901 14.0855 14.667 13.4972 14.667C12.9089 14.667 12.432 14.1901 12.432 13.6018C12.432 13.0135 12.9089 12.5366 13.4972 12.5366C14.0855 12.5366 14.5624 13.0135 14.5624 13.6018ZM7.1061 13.6018C7.1061 14.1901 6.6292 14.667 6.04091 14.667C5.45262 14.667 4.97572 14.1901 4.97572 13.6018C4.97572 13.0135 5.45262 12.5366 6.04091 12.5366C6.6292 12.5366 7.1061 13.0135 7.1061 13.6018ZM22.0187 13.6018C22.0187 14.1901 21.5418 14.667 20.9536 14.667C20.3653 14.667 19.8884 14.1901 19.8884 13.6018C19.8884 13.0135 20.3653 12.5366 20.9536 12.5366C21.5418 12.5366 22.0187 13.0135 22.0187 13.6018Z" stroke="#374957" stroke-width="2.13038" stroke-linecap="round"/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="22" viewBox="0 0 21 22" fill="none" class="task__close-icon pointer">
                      <g clip-path="url(#clip0_2_4937)">
                        <path d="M12.9999 13.4684L10.4701 10.9386M10.4701 10.9386L7.94026 8.40879M10.4701 10.9386L7.94026 13.4684M10.4701 10.9386L12.9999 8.40879M1.19406 10.9386C1.19406 16.0616 5.34708 20.2146 10.4701 20.2146C15.5931 20.2146 19.7461 16.0616 19.7461 10.9386C19.7461 5.81561 15.5931 1.6626 10.4701 1.6626C5.34708 1.6626 1.19406 5.81561 1.19406 10.9386Z" stroke="#374957" stroke-width="2.13038" stroke-linecap="round"/>
                      </g>
                      <defs>
                        <clipPath id="clip0_2_4937">
                          <rect width="20.2386" height="20.2386" fill="white" transform="translate(0.350815 0.81958)"/>
                        </clipPath>
                      </defs>
                    </svg>
                  </div>
                </div>
                <form class="task__modal__content">
                  <div class="task__modal__main">
                    <div class="task__modal__main__header">
                      <div class="task__modal__status pointer">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13" fill="none">
                          <circle cx="6.56458" cy="6.20874" r="6" fill="${statusColor}"/>
                        </svg>
                        <p class="task__modal__status__text">${currentStatus}</p>
                        <svg xmlns="http://www.w3.org/2000/svg" width="9" height="7" viewBox="0 0 9 7" fill="none">
                          <path d="M4.75663 5.97827L8.47112 1.52089C8.90534 0.999826 8.53481 0.20874 7.85654 0.20874L1.27261 0.20874C0.594338 0.20874 0.223813 0.999825 0.658031 1.52089L4.37252 5.97827C4.47247 6.09821 4.65668 6.09821 4.75663 5.97827Z" fill="${statusColor}"/>
                        </svg>
                      </div>
                      <img src="img/addAvatar.png" />
                      <div class="task__modal__priority">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="19" viewBox="0 0 17 19" fill="none">
                        <path d="M1.56458 1.20874V11.6373H14.2994C14.8345 11.6373 15.102 11.6373 15.1645 11.4792C15.227 11.3211 15.0317 11.1382 14.6413 10.7724L10.0429 6.46495C9.89682 6.32813 9.82379 6.25972 9.82379 6.17303C9.82379 6.08633 9.89682 6.01792 10.0429 5.8811L14.6413 1.57365C15.0317 1.20786 15.227 1.02497 15.1645 0.866853C15.102 0.70874 14.8345 0.70874 14.2994 0.70874H2.06458C1.82887 0.70874 1.71102 0.70874 1.6378 0.781964C1.56458 0.855187 1.56458 0.973038 1.56458 1.20874Z" fill="${priorityColor}"/>
                          <path d="M1.56458 11.6373V1.20874C1.56458 0.973038 1.56458 0.855187 1.6378 0.781964C1.71102 0.70874 1.82887 0.70874 2.06458 0.70874H14.2994C14.8345 0.70874 15.102 0.70874 15.1645 0.866853C15.227 1.02497 15.0317 1.20786 14.6413 1.57365L10.0429 5.8811C9.89682 6.01792 9.82379 6.08633 9.82379 6.17303C9.82379 6.25972 9.89682 6.32813 10.0429 6.46495L14.6413 10.7724C15.0317 11.1382 15.227 11.3211 15.1645 11.4792C15.102 11.6373 14.8345 11.6373 14.2994 11.6373H1.56458ZM1.56458 11.6373V17.7087" stroke="${priorityColor}" stroke-linecap="round"/>
                        </svg>
                        <p class="task__modal__priority__text">${priorityText}</p>
                      </div>
                      <img src="img/taskTime.png">
                    </div>

                    <div class="task__modal__main__body">
                      <div class="task__modal__main__body__header">
                        <svg xmlns="http://www.w3.org/2000/svg" width="2" height="25" viewBox="0 0 2 25" fill="none">
                          <path d="M0.564575 0.637451V24.6042" stroke="#CCD2E3" stroke-width="1.06519"/>
                        </svg>
                        <input type="text" placeholder="Add Task's name" class="task__modal__main__body__header__task-name" >
                      </div>
                      <div class="task__modal__main__body__header__task-description__container">
                        <svg xmlns="http://www.w3.org/2000/svg" width="19" height="18" viewBox="0 0 19 18" fill="none">
                          <path d="M10.9941 13.3913C10.9941 13.7985 11.3242 14.1285 11.7314 14.1285C12.1386 14.1285 12.4686 13.7985 12.4686 13.3913C12.4686 12.9841 12.1386 12.6541 11.7314 12.6541C11.3242 12.6541 10.9941 12.9841 10.9941 13.3913Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                          <path d="M6.57068 13.3913C6.57068 13.7985 6.90075 14.1285 7.30792 14.1285C7.71509 14.1285 8.04517 13.7985 8.04517 13.3913C8.04517 12.9841 7.71509 12.6541 7.30792 12.6541C6.90075 12.6541 6.57068 12.9841 6.57068 13.3913Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                          <path d="M10.9941 8.96783C10.9941 9.375 11.3242 9.70508 11.7314 9.70508C12.1386 9.70508 12.4686 9.375 12.4686 8.96783C12.4686 8.56067 12.1386 8.23059 11.7314 8.23059C11.3242 8.23059 10.9941 8.56067 10.9941 8.96783Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                          <path d="M6.57068 8.96783C6.57068 9.375 6.90075 9.70508 7.30792 9.70508C7.71509 9.70508 8.04517 9.375 8.04517 8.96783C8.04517 8.56067 7.71509 8.23059 7.30792 8.23059C6.90075 8.23059 6.57068 8.56067 6.57068 8.96783Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                          <path d="M10.9941 4.54437C10.9941 4.95154 11.3242 5.28162 11.7314 5.28162C12.1386 5.28162 12.4686 4.95154 12.4686 4.54437C12.4686 4.1372 12.1386 3.80713 11.7314 3.80713C11.3242 3.80713 10.9941 4.1372 10.9941 4.54437Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                          <path d="M6.57068 4.54437C6.57068 4.95154 6.90075 5.28162 7.30792 5.28162C7.71509 5.28162 8.04517 4.95154 8.04517 4.54437C8.04517 4.1372 7.71509 3.80713 7.30792 3.80713C6.90075 3.80713 6.57068 4.1372 6.57068 4.54437Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <textarea class="task__modal__main__body__header__task-description" placeholder="Add description"></textarea>
                      </div>
                      <img src="img/taskAddTags.png"/>
                      <div class="task__modal__attachement">
                        <p class="task__modal__attachement__text">Attachements</p>
                        <label class="task__modal__attachement__input"><svg xmlns="http://www.w3.org/2000/svg" width="27" height="26" viewBox="0 0 27 26" fill="none">
                        <path d="M15.4772 11.0322L11.2165 15.293" stroke="#222222" stroke-width="1.06519" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M17.6076 14.2279L19.738 12.0976C21.2087 10.6268 21.2087 8.24233 19.738 6.77162V6.77162C18.2672 5.3009 15.8827 5.3009 14.412 6.77161L12.2816 8.90199M9.08608 12.0976L6.9557 14.2279C5.48498 15.6987 5.48498 18.0832 6.9557 19.5539V19.5539C8.42642 21.0246 10.8109 21.0246 12.2816 19.5539L14.412 17.4235" stroke="#222222" stroke-width="1.06519" stroke-linecap="round"/>
                      </svg> <spna>Drag and drop files to attach or <span>Browse</span></spna><input type="file"></label>
                      </div>
                    </div>
                  </div>
                  <div class="task__modal__activity">
                    <div class="task__modal__activity__header">
                      <p class="task__modal__activity__heading">Activity</p>
                      <div class="task__modal__activity__content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="5" height="9" viewBox="0 0 5 9" fill="none">
                          <path d="M0.585429 7.78248V1.28413C0.584757 1.16857 0.618512 1.05544 0.682391 0.959143C0.74627 0.862849 0.837378 0.787759 0.9441 0.743448C1.05082 0.699136 1.16832 0.687611 1.28162 0.71034C1.39492 0.73307 1.49888 0.789025 1.58025 0.871071L4.82653 4.12316C4.93488 4.23216 4.9957 4.37961 4.9957 4.5333C4.9957 4.687 4.93488 4.83445 4.82653 4.94345L1.58025 8.19553C1.49888 8.27758 1.39492 8.33353 1.28162 8.35626C1.16832 8.37899 1.05082 8.36747 0.9441 8.32316C0.837378 8.27884 0.74627 8.20376 0.682391 8.10746C0.618512 8.01117 0.584757 7.89803 0.585429 7.78248Z" fill="#FFA948"/>
                        </svg>
                        <p class="task__modal__activity__content__text">${JSON.parse(localStorage.getItem("data")).username} create this task</p>
                      </div> 
                    </div>
                    <div class="task__modal__activity__form">
                      <input class="task__modal__activity__form__input" placeholder="Comment" />
                      <div class="btn--comment pointer">
                        <p>Envoyer</p>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="overlay overlay--task"></div>
            </div>
          `;
                    if (btnAddTaskHandler && document.querySelector(".modal") === null) document.querySelector("body").insertAdjacentHTML("afterbegin", html);
                    document.querySelector(".task__modal__status")?.addEventListener("click", function(e) {
                        e.stopPropagation();
                        const statusModal = `
                  <div class="status__window">
                    <div class="status__modal">
                      
                    </div>
                    <div class="status__overlay"></div>
                  </div>
                `;
                        if (document.querySelector(".status__window") === null) {
                            this.insertAdjacentHTML("afterbegin", statusModal);
                            projectStatuses.map((status)=>{
                                this.querySelector(".status__modal").insertAdjacentHTML("afterbegin", `
                    <p class="status__modal__el">
                      ${status.attributes.status}
                    </p>
                    `);
                            });
                            this.querySelector(".status__modal").addEventListener("click", function(e) {
                                e.stopPropagation();
                                console.log(e.target.textContent.trim());
                                this.closest(".task__modal__status").querySelector(".task__modal__status__text").textContent = e.target.textContent.trim();
                                this.closest(".status__window").remove();
                            });
                        }
                        document.querySelector(".status__overlay").addEventListener("click", function(e) {
                            e.stopPropagation();
                            this.closest(".status__window").remove();
                        });
                    // fetch for my statuses then render them iside the model
                    });
                    const closeModal = function() {
                        document.querySelector("form").addEventListener("submit", function(e) {
                            e.preventDefault();
                        });
                        if (document.querySelector(".add-task-window") !== null) document.addEventListener("keyup", function(e) {
                            const specificStatus = projectStatuses.filter((state)=>state.attributes.status === this.querySelector(".task__modal__status__text")?.textContent.trim())[0];
                            const requirement = this.querySelector(".task__modal__main__body__header__task-name")?.value && this.querySelector(".task__modal__main__body__header__task-description")?.value ? true : false;
                            if (e.code === "Enter" && requirement) {
                                // Here I must upload data to Strapi
                                saveTask(this.querySelector(".task__modal__main__body__header__task-name").value, this.querySelector(".task__modal__main__body__header__task-description").value, this.querySelector(".task__modal__priority__text").textContent.toLowerCase(), this.querySelector(".task__modal__attachement__input").querySelector("input").files[0], specificStatus, this.querySelector(".task__modal__status__text").textContent);
                                this.querySelector(".add-task-window").remove();
                            // window.location.reload();
                            }
                        });
                        document.querySelector(".overlay")?.addEventListener("click", function() {
                            // this.saveTask();
                            this.closest(".add-task-window").remove();
                        // window.location.reload();
                        }); //list
                        document.querySelector(".task__close-icon")?.addEventListener("click", function() {
                            // this.saveTask();
                            this.closest(".add-task-window").remove();
                        // window.location.reload();
                        });
                    };
                    closeModal();
                }, 200);
            });
        });
    }
}
exports.default = new AddTaskView();

},{"./../config.js":"k5Hzs","./../helpers.js":"hGI1E","./../model.js":"Y4A21","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"kthUK":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
class ProjectLocationView {
    parent = document.querySelector(".header__location__title");
    renderProjectLocation() {
        console.log(document.querySelector(".navigation-bar__project--selected"));
        window.addEventListener("click", function() {
            const project = document.querySelector(".navigation-bar__project--selected")?.querySelector(".project__title__el")?.querySelector("p")?.textContent;
            console.log(project);
            if (project) document.querySelector(".header__location").querySelector(".header__location__title").textContent = `Project > ${project}`;
        });
    }
}
exports.default = new ProjectLocationView();

},{"@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"8wuHO":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
class CardNavView {
    elements = document.querySelectorAll(".card__nav__link");
    renderCardNav() {
        this.elements.forEach((el)=>{
            el.addEventListener("click", function() {
                this.closest(".card__nav").querySelector(".card__nav__link--active").classList.remove("card__nav__link--active");
                el.classList.add("card__nav__link--active");
            });
        });
    }
}
exports.default = new CardNavView();

},{"@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"gEib3":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
var _modelJs = require("./../model.js");
var _cardListViewJs = require("./cardListView.js");
var _cardListViewJsDefault = parcelHelpers.interopDefault(_cardListViewJs);
var _helpersJs = require("../helpers.js");
var _configJs = require("../config.js");
var _cardBoardViewJs = require("./cardBoardView.js");
var _cardBoardViewJsDefault = parcelHelpers.interopDefault(_cardBoardViewJs);
class CardView {
    async renderCard(myProjects) {
        console.log(document.querySelector(".navigation-bar__project--selected"));
        if (document.querySelector(".navigation-bar__project--selected") === null) return;
        const project = myProjects.filter((proj)=>proj.attributes.name === document.querySelector(".navigation-bar__project--selected").querySelector(".project__title__el").querySelector("p").textContent);
        console.log(project);
        const specificProject = await (0, _helpersJs.AJAX)(`${(0, _configJs.API_URL)}/projects/${project[0].id}/?populate=*`);
        console.log(specificProject);
        const specificStatuses = specificProject.data.attributes.statuses.data.map((status)=>status.id);
        let statuses = [];
        specificStatuses.map(async (stateId)=>{
            const data = await (0, _helpersJs.AJAX)(`${(0, _configJs.API_URL)}/statuses/${stateId}/?populate=*`);
            console.log(data.data);
            statuses = [
                ...statuses,
                data.data
            ];
        });
        setTimeout(()=>{
            console.log(statuses);
            const view = document.querySelector(".card__nav__link--active").querySelector("span").textContent;
            if (view === "List") {
                console.log("List View");
                (0, _cardListViewJsDefault.default).renderCardList(specificProject, myProjects, statuses);
            }
            if (view === "Board") {
                console.log("Board View");
                (0, _cardBoardViewJsDefault.default).renderCardBoard(specificProject, myProjects, statuses);
            }
        }, 100);
    }
}
exports.default = new CardView();

},{"./../model.js":"Y4A21","./cardListView.js":"2z7Ju","../helpers.js":"hGI1E","../config.js":"k5Hzs","./cardBoardView.js":"fTAko","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"2z7Ju":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
var _modelJs = require("./../model.js");
var _configJs = require("../config.js");
var _helpersJs = require("../helpers.js");
var _addTaskViewJs = require("./addTaskView.js");
var _addTaskViewJsDefault = parcelHelpers.interopDefault(_addTaskViewJs);
var _addStatusViewJs = require("./addStatusView.js");
var _addStatusViewJsDefault = parcelHelpers.interopDefault(_addStatusViewJs);
class CardListView {
    renderCardList(specificProject, myProjects, specificStatuses) {
        const parent = document.querySelector(".card__container");
        if (specificProject.length === 0) parent.innerHTML = "";
        const html = `
      <div class="card__list__view margin-top">
        <div class="card__list__view__status__container">
        
        </div>
        
        <div class="card__add-status pointer">
          <svg xmlns="http://www.w3.org/2000/svg" width="18" height="19" viewBox="0 0 18 19" fill="none">
            <g clip-path="url(#clip0_1416_1521)">
              <path d="M9 0.5C7.21997 0.5 5.47991 1.02784 3.99987 2.01677C2.51983 3.00571 1.36628 4.41131 0.685088 6.05585C0.00389957 7.70038 -0.17433 9.50998 0.172937 11.2558C0.520204 13.0016 1.37737 14.6053 2.63604 15.864C3.89472 17.1226 5.49836 17.9798 7.24419 18.3271C8.99002 18.6743 10.7996 18.4961 12.4442 17.8149C14.0887 17.1337 15.4943 15.9802 16.4832 14.5001C17.4722 13.0201 18 11.28 18 9.5C17.9974 7.11384 17.0484 4.82616 15.3611 3.13889C13.6738 1.45162 11.3862 0.502581 9 0.5V0.5ZM9 17C7.51664 17 6.0666 16.5601 4.83323 15.736C3.59986 14.9119 2.63856 13.7406 2.07091 12.3701C1.50325 10.9997 1.35473 9.49168 1.64411 8.03682C1.9335 6.58197 2.64781 5.24559 3.6967 4.1967C4.7456 3.14781 6.08197 2.4335 7.53683 2.14411C8.99168 1.85472 10.4997 2.00325 11.8701 2.5709C13.2406 3.13856 14.4119 4.09985 15.236 5.33322C16.0601 6.56659 16.5 8.01664 16.5 9.5C16.4978 11.4885 15.7069 13.3948 14.3009 14.8009C12.8948 16.2069 10.9885 16.9978 9 17ZM12.75 9.5C12.75 9.69891 12.671 9.88968 12.5303 10.0303C12.3897 10.171 12.1989 10.25 12 10.25H9.75V12.5C9.75 12.6989 9.67099 12.8897 9.53033 13.0303C9.38968 13.171 9.19892 13.25 9 13.25C8.80109 13.25 8.61033 13.171 8.46967 13.0303C8.32902 12.8897 8.25 12.6989 8.25 12.5V10.25H6C5.80109 10.25 5.61033 10.171 5.46967 10.0303C5.32902 9.88968 5.25 9.69891 5.25 9.5C5.25 9.30109 5.32902 9.11032 5.46967 8.96967C5.61033 8.82902 5.80109 8.75 6 8.75H8.25V6.5C8.25 6.30109 8.32902 6.11032 8.46967 5.96967C8.61033 5.82902 8.80109 5.75 9 5.75C9.19892 5.75 9.38968 5.82902 9.53033 5.96967C9.67099 6.11032 9.75 6.30109 9.75 6.5V8.75H12C12.1989 8.75 12.3897 8.82902 12.5303 8.96967C12.671 9.11032 12.75 9.30109 12.75 9.5Z" fill="#374957"/>
            </g>
            <defs>
              <clipPath id="clip0_1416_1521">
                <rect width="18" height="18" fill="white" transform="translate(0 0.5)"/>
              </clipPath>
            </defs>
          </svg>
          <p>New State</p>
        </div>
      </div>
    `;
        parent.innerHTML = "";
        parent.insertAdjacentHTML("afterbegin", html);
        //  Add status view
        document.querySelector(".card__add-status").addEventListener("click", function(e) {
            e.stopPropagation();
            (0, _addStatusViewJsDefault.default).renderStatusModal();
        });
        let tasksOfProject = 0;
        // Add specific status to the correspondent project
        specificStatuses.map((status)=>{
            document.querySelector(".card__list__view__status__container").insertAdjacentHTML("beforeend", `
      <div class="card__list__view__block">
        <div class="card__list__view__block__header">
          <div class="card__list__view__block__header__status">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
              <path d="M12.1921 15.2695L15.9065 10.8121C16.3408 10.2911 15.9702 9.5 15.292 9.5H8.70803C8.02976 9.5 7.65924 10.2911 8.09346 10.8121L11.8079 15.2695C11.9079 15.3895 12.0921 15.3895 12.1921 15.2695Z" fill="#787878"/>
            </svg>
            <div class="card__list__view__block__header__status__text__container">
              <svg xmlns="http://www.w3.org/2000/svg" width="12" height="13" viewBox="0 0 12 13" fill="none">
                <circle cx="6" cy="6.5" r="6" fill="#C5C5C5"/>
              </svg>
              <p class="card__list__view__block__header__status__text">
                ${status.attributes.status}
              </p>
              <p class="card__list__view__block__header__status__list-count">
                ${status.attributes?.tasks?.data?.length}
              </p>
            </div>
            <div class="card__view__add-task pointer">
              <p>+ New task</p>
            </div>
          </div>
          <div class="card__list__view__block__header__details">
            <p>Assignee</p>
            <p>Due Data</p>
            <p>Priority</p>
            <p>More</p>
          </div>
          
        </div>
        <div class="card__list__view__tasks" data-status="${status.attributes.status}">
              
        </div>
      </div>
      `);
            // document.querySelector('.card__list__view__tasks').innerHTML = '';
            tasksOfProject = status.attributes?.tasks?.data.length + tasksOfProject;
            status.attributes?.tasks?.data.map(async (task)=>{
                const taskStatus = await (0, _helpersJs.AJAX)(`${(0, _configJs.API_URL)}/tasks/${task.id}/?populate=*`);
                console.log(task, document.querySelector(".card__list__view__tasks"));
                console.log();
                document.querySelectorAll(".card__list__view__tasks").forEach((stateField)=>{
                    console.log(stateField.dataset.status);
                    console.log(stateField.dataset.status === taskStatus.data.attributes.statuses.data[0].attributes.status);
                    if (stateField.dataset.status === taskStatus.data.attributes.statuses.data[0].attributes.status) {
                        console.log(stateField);
                        stateField.insertAdjacentHTML("afterbegin", `<div class="card__list__view__task" draggable="true" data-id="${task.id}">
              <div class="card__list__view__task__name__container">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
                  <path d="M14 18.5C14 19.0523 14.4477 19.5 15 19.5C15.5523 19.5 16 19.0523 16 18.5C16 17.9477 15.5523 17.5 15 17.5C14.4477 17.5 14 17.9477 14 18.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M8 18.5C8 19.0523 8.44772 19.5 9 19.5C9.55228 19.5 10 19.0523 10 18.5C10 17.9477 9.55228 17.5 9 17.5C8.44772 17.5 8 17.9477 8 18.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M14 12.5C14 13.0523 14.4477 13.5 15 13.5C15.5523 13.5 16 13.0523 16 12.5C16 11.9477 15.5523 11.5 15 11.5C14.4477 11.5 14 11.9477 14 12.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M8 12.5C8 13.0523 8.44772 13.5 9 13.5C9.55228 13.5 10 13.0523 10 12.5C10 11.9477 9.55228 11.5 9 11.5C8.44772 11.5 8 11.9477 8 12.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M14 6.5C14 7.05228 14.4477 7.5 15 7.5C15.5523 7.5 16 7.05228 16 6.5C16 5.94772 15.5523 5.5 15 5.5C14.4477 5.5 14 5.94772 14 6.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M8 6.5C8 7.05228 8.44772 7.5 9 7.5C9.55228 7.5 10 7.05228 10 6.5C10 5.94772 9.55228 5.5 9 5.5C8.44772 5.5 8 5.94772 8 6.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
                  <path d="M14.7695 12.3079L10.3121 8.59346C9.79109 8.15924 9 8.52976 9 9.20803V15.792C9 16.4702 9.79109 16.8408 10.3121 16.4065L14.7695 12.6921C14.8895 12.5921 14.8895 12.4079 14.7695 12.3079Z" fill="#AFAFAF"/>
                </svg>
                <div class="card__list__view__task__div">
                  <svg xmlns="http://www.w3.org/2000/svg" width="10" height="11" viewBox="0 0 10 11" fill="none">
                    <circle cx="5" cy="5.5" r="5" fill="#C5C5C5"/>
                  </svg>
                  <p>${task.attributes.title}</p>
                </div>
              </div>
              <div class="card__list__view__task__details"></div>
            </div>`);
                    }
                });
                console.log(task.id);
                console.log(taskStatus.data.attributes.statuses.data[0].attributes.status);
                ///////////////////////////////////////////////////////////
                // Drag && Drop To change completly
                // To Do: Drag & Drop Tasks between multiple statuses
                console.log(tasksOfProject, document.querySelectorAll(".card__list__view__task").length);
                if (tasksOfProject === document.querySelectorAll(".card__list__view__task").length) {
                    const draggables = document.querySelectorAll(".card__list__view__task");
                    const containers = document.querySelectorAll(".card__list__view__tasks");
                    const getDragAfterElement = function(container, y) {
                        const draggableElements = [
                            ...container.querySelectorAll(".card__list__view__task:not(.dragging)")
                        ];
                        return draggableElements.reduce((closest, child)=>{
                            const box = child.getBoundingClientRect();
                            const offset = y - box.top - box.height / 2;
                            if (offset < 0 && offset > closest.offset) return {
                                offset: offset,
                                element: child
                            };
                            else return closest;
                        }, {
                            offset: Number.NEGATIVE_INFINITY
                        }).element;
                    };
                    draggables.forEach((draggable)=>{
                        draggable.addEventListener("dragstart", function() {
                            draggable.classList.add("dragging");
                        });
                        draggable.addEventListener("dragend", async function() {
                            draggable.classList.remove("dragging");
                            // debugger;
                            document.querySelectorAll(".card__list__view__tasks").forEach(async (statusList)=>{
                                console.log(statusList);
                                // statusList.innerHTML = '';
                                const currentPosition = statusList.querySelectorAll(".card__list__view__task");
                                console.log(currentPosition);
                                const newTasksOrder = {
                                    data: {
                                        tasks: []
                                    }
                                };
                                currentPosition.forEach((draggable)=>{
                                    newTasksOrder.data.tasks.unshift({
                                        id: parseInt(draggable.dataset.id)
                                    });
                                });
                                // console.log(myProject);
                                const stateDrop = specificStatuses.find((el)=>el.attributes.status === statusList.dataset.status);
                                await _modelJs.updateRelations(`${(0, _configJs.API_URL)}/statuses/${stateDrop.id}`, newTasksOrder);
                            });
                            document.querySelector("body").click();
                        });
                    });
                    containers.forEach((container)=>{
                        container.addEventListener("dragover", function(e) {
                            e.preventDefault();
                            const afterElement = getDragAfterElement(container, e.clientY);
                            const draggable = document.querySelector(".dragging");
                            if (afterElement == null) container.appendChild(draggable);
                            else container.insertBefore(draggable, afterElement);
                        });
                    });
                }
            ///////////////////////////////////
            });
        });
        parent.querySelectorAll(".card__view__add-task").forEach((el)=>{
            el.addEventListener("click", function() {
                const btnClicked = true;
                (0, _addTaskViewJsDefault.default).addTask(myProjects, btnClicked);
                this.click();
            });
        });
    }
}
exports.default = new CardListView();

},{"./../model.js":"Y4A21","../config.js":"k5Hzs","../helpers.js":"hGI1E","./addTaskView.js":"dKG6e","./addStatusView.js":"citeD","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"citeD":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
var _modelJs = require("./../model.js");
var _helpersJs = require("../helpers.js");
var _configJs = require("../config.js");
var _projectViewJs = require("./projectView.js");
var _projectViewJsDefault = parcelHelpers.interopDefault(_projectViewJs);
class CardView {
    renderStatusModal() {
        const parent = document.querySelector(".card__add-status");
        // status design
        const html = `
      <div class="add-status__modal">
        <div>
          <input class="add-status__modal__input" placeholder="Status name...">
        </div>
        <div class="add-status__modal__submit pointer">
          <svg xmlns="http://www.w3.org/2000/svg" width="17" height="18" viewBox="0 0 17 18" fill="none">
            <path d="M10.625 6.16667H6.09167C5.69496 6.16667 5.49661 6.16667 5.34509 6.08946C5.21181 6.02155 5.10345 5.91319 5.03554 5.77991C4.95833 5.62839 4.95833 5.43004 4.95833 5.03333V2.625M12.0417 15.375V10.8417C12.0417 10.445 12.0417 10.2466 11.9645 10.0951C11.8966 9.96181 11.7882 9.85345 11.6549 9.78554C11.5034 9.70833 11.305 9.70833 10.9083 9.70833H6.09167C5.69496 9.70833 5.49661 9.70833 5.34509 9.78554C5.21181 9.85345 5.10345 9.96181 5.03554 10.0951C4.95833 10.2466 4.95833 10.445 4.95833 10.8417V15.375M14.875 7.10555V11.975C14.875 13.1651 14.875 13.7602 14.6434 14.2147C14.4397 14.6146 14.1146 14.9397 13.7147 15.1434C13.2602 15.375 12.6651 15.375 11.475 15.375H5.525C4.33489 15.375 3.73983 15.375 3.28527 15.1434C2.88543 14.9397 2.56034 14.6146 2.35661 14.2147C2.125 13.7602 2.125 13.1651 2.125 11.975V6.025C2.125 4.83489 2.125 4.23983 2.35661 3.78527C2.56034 3.38543 2.88543 3.06034 3.28527 2.85661C3.73983 2.625 4.33489 2.625 5.525 2.625H10.3944C10.741 2.625 10.9142 2.625 11.0772 2.66414C11.2218 2.69885 11.36 2.75609 11.4867 2.83376C11.6297 2.92137 11.7522 3.04388 11.9972 3.28889L14.2111 5.50278C14.4561 5.74779 14.5786 5.8703 14.6662 6.01326C14.7439 6.14002 14.8012 6.2782 14.8359 6.42275C14.875 6.5858 14.875 6.75905 14.875 7.10555Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
          </svg>
          <p>Save</p>
        </div>
      </div>
    `;
        console.log(parent);
        if (document.querySelector(".add-status__modal")) document.querySelector(".add-status__modal").remove();
        let statusModalIsDisplayed;
        parent.insertAdjacentHTML("afterend", html);
        if (document.querySelector(".add-status__modal")) {
            document.querySelector(".add-status__modal").addEventListener("click", function(e) {
                e.stopPropagation();
            });
            statusModalIsDisplayed = true;
        }
        if (statusModalIsDisplayed) parent.style.background = "#bde0f4";
        // add status to strapi using POST method AJAX
        document.querySelector(".add-status__modal__submit").addEventListener("click", async function(e) {
            e.stopPropagation();
            console.log(this.closest(".add-status__modal").querySelector(".add-status__modal__input").value);
            console.log(this.closest("body").querySelector(".navigation-bar__project--selected").querySelector(".project__title__el").querySelector("p").textContent);
            const myProj = await _modelJs.findMyData(JSON.parse(localStorage.getItem("data")), "projects");
            console.log(myProj);
            myProj.map((project)=>(0, _projectViewJsDefault.default).renderProject(project.attributes, myProj));
            const proj = myProj.filter((proj)=>proj.attributes.name === document.querySelector(".navigation-bar__project--selected").querySelector(".project__title__el").querySelector("p").textContent)[0];
            console.log(proj);
            const filtredStatuses = await _modelJs.findMyData(proj, "statuses");
            console.log(filtredStatuses);
            const status = {
                data: {
                    status: this.closest(".add-status__modal").querySelector(".add-status__modal__input").value
                }
            };
            const currentStatus = await (0, _helpersJs.AJAX)(`${(0, _configJs.API_URL)}/statuses`, status);
            const statuses = {
                data: {
                    statuses: [
                        ...filtredStatuses,
                        currentStatus.data
                    ]
                }
            };
            await _modelJs.updateRelations(`${(0, _configJs.API_URL)}/projects/${proj.id}`, statuses);
            document.querySelector("body").click();
            this.closest(".add-status__modal").remove();
        });
    }
}
exports.default = new CardView();

},{"./../model.js":"Y4A21","../helpers.js":"hGI1E","../config.js":"k5Hzs","./projectView.js":"buQla","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"fTAko":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
var _modelJs = require("./../model.js");
var _configJs = require("../config.js");
var _helpersJs = require("../helpers.js");
var _addTaskViewJs = require("./addTaskView.js");
var _addTaskViewJsDefault = parcelHelpers.interopDefault(_addTaskViewJs);
var _addStatusViewJs = require("./addStatusView.js");
var _addStatusViewJsDefault = parcelHelpers.interopDefault(_addStatusViewJs);
class CardBoardView {
    renderCardBoard(specificProject, myProjects, specificStatuses) {
        const parent = document.querySelector(".card__container");
        if (specificProject.length === 0) parent.innerHTML = "";
        const html = `
      <div class="card__board__view margin-top">
        <div class="card__board__view__status__container">
        
        </div>
        
        <div class="card__add-status pointer">
          <svg xmlns="http://www.w3.org/2000/svg" width="18" height="19" viewBox="0 0 18 19" fill="none">
            <g clip-path="url(#clip0_1416_1521)">
              <path d="M9 0.5C7.21997 0.5 5.47991 1.02784 3.99987 2.01677C2.51983 3.00571 1.36628 4.41131 0.685088 6.05585C0.00389957 7.70038 -0.17433 9.50998 0.172937 11.2558C0.520204 13.0016 1.37737 14.6053 2.63604 15.864C3.89472 17.1226 5.49836 17.9798 7.24419 18.3271C8.99002 18.6743 10.7996 18.4961 12.4442 17.8149C14.0887 17.1337 15.4943 15.9802 16.4832 14.5001C17.4722 13.0201 18 11.28 18 9.5C17.9974 7.11384 17.0484 4.82616 15.3611 3.13889C13.6738 1.45162 11.3862 0.502581 9 0.5V0.5ZM9 17C7.51664 17 6.0666 16.5601 4.83323 15.736C3.59986 14.9119 2.63856 13.7406 2.07091 12.3701C1.50325 10.9997 1.35473 9.49168 1.64411 8.03682C1.9335 6.58197 2.64781 5.24559 3.6967 4.1967C4.7456 3.14781 6.08197 2.4335 7.53683 2.14411C8.99168 1.85472 10.4997 2.00325 11.8701 2.5709C13.2406 3.13856 14.4119 4.09985 15.236 5.33322C16.0601 6.56659 16.5 8.01664 16.5 9.5C16.4978 11.4885 15.7069 13.3948 14.3009 14.8009C12.8948 16.2069 10.9885 16.9978 9 17ZM12.75 9.5C12.75 9.69891 12.671 9.88968 12.5303 10.0303C12.3897 10.171 12.1989 10.25 12 10.25H9.75V12.5C9.75 12.6989 9.67099 12.8897 9.53033 13.0303C9.38968 13.171 9.19892 13.25 9 13.25C8.80109 13.25 8.61033 13.171 8.46967 13.0303C8.32902 12.8897 8.25 12.6989 8.25 12.5V10.25H6C5.80109 10.25 5.61033 10.171 5.46967 10.0303C5.32902 9.88968 5.25 9.69891 5.25 9.5C5.25 9.30109 5.32902 9.11032 5.46967 8.96967C5.61033 8.82902 5.80109 8.75 6 8.75H8.25V6.5C8.25 6.30109 8.32902 6.11032 8.46967 5.96967C8.61033 5.82902 8.80109 5.75 9 5.75C9.19892 5.75 9.38968 5.82902 9.53033 5.96967C9.67099 6.11032 9.75 6.30109 9.75 6.5V8.75H12C12.1989 8.75 12.3897 8.82902 12.5303 8.96967C12.671 9.11032 12.75 9.30109 12.75 9.5Z" fill="#374957"/>
            </g>
            <defs>
              <clipPath id="clip0_1416_1521">
                <rect width="18" height="18" fill="white" transform="translate(0 0.5)"/>
              </clipPath>
            </defs>
          </svg>
          <p>New State</p>
        </div>
      </div>
    `;
        parent.innerHTML = "";
        parent.insertAdjacentHTML("afterbegin", html);
        //  Add status view
        document.querySelector(".card__add-status").addEventListener("click", function(e) {
            e.stopPropagation();
            (0, _addStatusViewJsDefault.default).renderStatusModal();
        });
        let tasksOfProject = 0;
        // Add specific status to the correspondent project
        specificStatuses.map((status)=>{
            document.querySelector(".card__board__view__status__container").insertAdjacentHTML("beforeend", `
      <div class="card__board__view__block">
        <div class="card__board__view__block__header">
          <div class="card__board__view__block__header__status">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
              <path d="M12.1921 15.2695L15.9065 10.8121C16.3408 10.2911 15.9702 9.5 15.292 9.5H8.70803C8.02976 9.5 7.65924 10.2911 8.09346 10.8121L11.8079 15.2695C11.9079 15.3895 12.0921 15.3895 12.1921 15.2695Z" fill="#787878"/>
            </svg>
            <div class="card__board__view__block__header__status__text__container">
              <svg xmlns="http://www.w3.org/2000/svg" width="12" height="13" viewBox="0 0 12 13" fill="none">
                <circle cx="6" cy="6.5" r="6" fill="#C5C5C5"/>
              </svg>
              <p class="card__board__view__block__header__status__text">
                ${status.attributes.status}
              </p>
              <p class="card__board__view__block__header__status__board-count">
                ${status.attributes?.tasks?.data?.length}
              </p>
            </div>
            <div class="card__view__add-task pointer">
              <p>+ New task</p>
            </div>
          </div>
          <div class="card__board__view__block__header__details">
            <p>Assignee</p>
            <p>Due Data</p>
            <p>Priority</p>
            <p>More</p>
          </div>
          
        </div>
        <div class="card__board__view__tasks" data-status="${status.attributes.status}">
              
        </div>
      </div>
      `);
            // document.querySelector('.card__board__view__tasks').innerHTML = '';
            tasksOfProject = status.attributes?.tasks?.data.length + tasksOfProject;
            status.attributes?.tasks?.data.map(async (task)=>{
                const taskStatus = await (0, _helpersJs.AJAX)(`${(0, _configJs.API_URL)}/tasks/${task.id}/?populate=*`);
                console.log(task, document.querySelector(".card__board__view__tasks"));
                console.log();
                document.querySelectorAll(".card__board__view__tasks").forEach((stateField)=>{
                    console.log(stateField.dataset.status);
                    console.log(stateField.dataset.status === taskStatus.data.attributes.statuses.data[0].attributes.status);
                    if (stateField.dataset.status === taskStatus.data.attributes.statuses.data[0].attributes.status) {
                        console.log(stateField);
                        stateField.insertAdjacentHTML("afterbegin", `<div class="card__board__view__task" draggable="true" data-id="${task.id}">
              <div class="card__board__view__task__name__container">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
                  <path d="M14 18.5C14 19.0523 14.4477 19.5 15 19.5C15.5523 19.5 16 19.0523 16 18.5C16 17.9477 15.5523 17.5 15 17.5C14.4477 17.5 14 17.9477 14 18.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M8 18.5C8 19.0523 8.44772 19.5 9 19.5C9.55228 19.5 10 19.0523 10 18.5C10 17.9477 9.55228 17.5 9 17.5C8.44772 17.5 8 17.9477 8 18.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M14 12.5C14 13.0523 14.4477 13.5 15 13.5C15.5523 13.5 16 13.0523 16 12.5C16 11.9477 15.5523 11.5 15 11.5C14.4477 11.5 14 11.9477 14 12.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M8 12.5C8 13.0523 8.44772 13.5 9 13.5C9.55228 13.5 10 13.0523 10 12.5C10 11.9477 9.55228 11.5 9 11.5C8.44772 11.5 8 11.9477 8 12.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M14 6.5C14 7.05228 14.4477 7.5 15 7.5C15.5523 7.5 16 7.05228 16 6.5C16 5.94772 15.5523 5.5 15 5.5C14.4477 5.5 14 5.94772 14 6.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M8 6.5C8 7.05228 8.44772 7.5 9 7.5C9.55228 7.5 10 7.05228 10 6.5C10 5.94772 9.55228 5.5 9 5.5C8.44772 5.5 8 5.94772 8 6.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
                  <path d="M14.7695 12.3079L10.3121 8.59346C9.79109 8.15924 9 8.52976 9 9.20803V15.792C9 16.4702 9.79109 16.8408 10.3121 16.4065L14.7695 12.6921C14.8895 12.5921 14.8895 12.4079 14.7695 12.3079Z" fill="#AFAFAF"/>
                </svg>
                <div class="card__board__view__task__div">
                  <svg xmlns="http://www.w3.org/2000/svg" width="10" height="11" viewBox="0 0 10 11" fill="none">
                    <circle cx="5" cy="5.5" r="5" fill="#C5C5C5"/>
                  </svg>
                  <p>${task.attributes.title}</p>
                </div>
              </div>
              <div class="card__board__view__task__details"></div>
            </div>`);
                    }
                });
                console.log(task.id);
                console.log(taskStatus.data.attributes.statuses.data[0].attributes.status);
                ///////////////////////////////////////////////////////////
                // Drag && Drop To change completly
                // To Do: Drag & Drop Tasks between multiple statuses
                console.log(tasksOfProject, document.querySelectorAll(".card__board__view__task").length);
                if (tasksOfProject === document.querySelectorAll(".card__board__view__task").length) {
                    const draggables = document.querySelectorAll(".card__board__view__task");
                    const containers = document.querySelectorAll(".card__board__view__tasks");
                    const getDragAfterElement = function(container, y) {
                        const draggableElements = [
                            ...container.querySelectorAll(".card__board__view__task:not(.dragging)")
                        ];
                        return draggableElements.reduce((closest, child)=>{
                            const box = child.getBoundingClientRect();
                            const offset = y - box.top - box.height / 2;
                            if (offset < 0 && offset > closest.offset) return {
                                offset: offset,
                                element: child
                            };
                            else return closest;
                        }, {
                            offset: Number.NEGATIVE_INFINITY
                        }).element;
                    };
                    draggables.forEach((draggable)=>{
                        draggable.addEventListener("dragstart", function() {
                            draggable.classList.add("dragging");
                        });
                        draggable.addEventListener("dragend", async function() {
                            draggable.classList.remove("dragging");
                            // debugger;
                            document.querySelectorAll(".card__board__view__tasks").forEach(async (statusList)=>{
                                console.log(statusList);
                                // statusList.innerHTML = '';
                                const currentPosition = statusList.querySelectorAll(".card__board__view__task");
                                console.log(currentPosition);
                                const newTasksOrder = {
                                    data: {
                                        tasks: []
                                    }
                                };
                                currentPosition.forEach((draggable)=>{
                                    newTasksOrder.data.tasks.unshift({
                                        id: parseInt(draggable.dataset.id)
                                    });
                                });
                                // console.log(myProject);
                                const stateDrop = specificStatuses.find((el)=>el.attributes.status === statusList.dataset.status);
                                await _modelJs.updateRelations(`${(0, _configJs.API_URL)}/statuses/${stateDrop.id}`, newTasksOrder);
                            });
                            document.querySelector("body").click();
                        });
                    });
                    containers.forEach((container)=>{
                        container.addEventListener("dragover", function(e) {
                            e.preventDefault();
                            const afterElement = getDragAfterElement(container, e.clientY);
                            const draggable = document.querySelector(".dragging");
                            if (afterElement == null) container.appendChild(draggable);
                            else container.insertBefore(draggable, afterElement);
                        });
                    });
                }
            ///////////////////////////////////
            });
        });
        parent.querySelectorAll(".card__view__add-task").forEach((el)=>{
            el.addEventListener("click", function(e) {
                const btnClicked = true;
                (0, _addTaskViewJsDefault.default).addTask(myProjects, btnClicked);
                this.click();
            });
        });
    }
}
exports.default = new CardBoardView();

},{"./../model.js":"Y4A21","../config.js":"k5Hzs","../helpers.js":"hGI1E","./addTaskView.js":"dKG6e","./addStatusView.js":"citeD","@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}],"hkjus":[function(require,module,exports) {
var parcelHelpers = require("@parcel/transformer-js/src/esmodule-helpers.js");
parcelHelpers.defineInteropFlag(exports);
class LogoutView {
    element = document.querySelector(".log-out");
    logout() {
        this.element.addEventListener("click", function() {
            localStorage.clear();
            window.location.reload();
        });
    }
}
exports.default = new LogoutView();

},{"@parcel/transformer-js/src/esmodule-helpers.js":"gkKU3"}]},["kYpTN","aenu9"], "aenu9", "parcelRequire7994")

//# sourceMappingURL=signup.e37f48ea.js.map
