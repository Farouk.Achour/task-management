import { API_URL, TIMEOUT_SEC } from './config.js';
import { AJAXaccess, AJAX, timeout } from './helpers.js';

export const state = [];

export const uploadUser = async function (data) {
  try {
    await AJAXaccess(`${API_URL}/auth/local/register`, data);
    state = AJAX(`${API_URL}/users`);
  } catch (err) {
    return `${err.message}`;
  }
};

export const findUser = async function (data) {
  try {
    await AJAXaccess(`${API_URL}/auth/local`, data);
  } catch (err) {
    return `${err.message}`;
  }
};

export const findUsers = async function () {
  try {
    const fetchPro = fetch(`${API_URL}/users`);
    const res = await Promise.race([fetchPro, timeout(TIMEOUT_SEC)]);
    const data = await res.json();

    if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);
    return data;
  } catch (err) {
    throw err;
  }
};

//  Refactor update user and project into a single function
export const updateRelations = async function (url, uploadData = undefined) {
  try {
    const fetchPro = fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(uploadData),
    });
    const res = await Promise.race([fetchPro, timeout(TIMEOUT_SEC)]);
    const data = await res.json();

    if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);

    return data;
  } catch (err) {
    throw err;
  }
};

export const findMyData = async function (parent, dataType) {
  try {
    const fetchPro = fetch(`${API_URL}/${dataType}?populate=*`);
    const res = await Promise.race([fetchPro, timeout(TIMEOUT_SEC)]);
    const data = await res.json();

    if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);

    const allData = data.data;
    let filteredData;
    switch (dataType) {
      case 'projects':
        filteredData = allData.reduce((acc, cur) => {
          cur.attributes.users.data[0]?.id === parent.id
            ? acc.push(cur)
            : console.log(acc);
          console.log(acc);
          return acc;
        }, []);
        break;
      case 'tasks':
        filteredData = allData.reduce((acc, cur) => {
          cur.attributes.statuses.data[0]?.id === parent.id
            ? acc.push(cur)
            : console.log(acc);
          return acc;
        }, []);
        break;
      case 'statuses':
        filteredData = allData.reduce((acc, cur) => {
          console.log(cur);
          cur.attributes.projects.data[0]?.id === parent.id
            ? acc.push(cur)
            : console.log(acc);
          return acc;
        }, []);
        break;
    }

    return filteredData;
  } catch (err) {
    throw err;
  }
};
