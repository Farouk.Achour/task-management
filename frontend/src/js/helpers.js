import { TIMEOUT_SEC } from './config';
import * as model from './model';

export const timeout = function (s) {
  return new Promise(function (_, reject) {
    setTimeout(function () {
      reject(new Error(`Request took too long! Timeout after ${s} second`));
    }, s * 1000);
  });
};

export const AJAXaccess = async function (url, uploadData = undefined) {
  try {
    const fetchPro = uploadData
      ? fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(uploadData),
        })
      : fetch(url);
    const res = await Promise.race([fetchPro, timeout(TIMEOUT_SEC)]);
    console.log(res);
    const data = await res.json();
    console.log(data);
    if (data.data !== null) {
      localStorage.setItem('data', JSON.stringify(data.user));
      localStorage.setItem('jwt', data.jwt);
      localStorage.setItem('user', data.user.id);
      console.log(data);
      model.state.push(data.user);
    }

    if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);
    return data;
  } catch (err) {
    throw err;
  }
};

export const AJAX = async function (url, uploadData = undefined) {
  try {
    console.log(uploadData, url);
    const fetchPro = uploadData
      ? fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(uploadData),
        })
      : fetch(url);
    const res = await Promise.race([fetchPro, timeout(TIMEOUT_SEC)]);
    console.log(res);
    const data = await res.json();
    console.log(data);

    if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);
    return data;
  } catch (err) {
    throw err;
  }
};
