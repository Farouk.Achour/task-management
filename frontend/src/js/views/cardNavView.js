class CardNavView {
  elements = document.querySelectorAll('.card__nav__link');

  renderCardNav() {
    this.elements.forEach(el => {
      el.addEventListener('click', function () {
        this.closest('.card__nav')
          .querySelector('.card__nav__link--active')
          .classList.remove('card__nav__link--active');
        el.classList.add('card__nav__link--active');
      });
    });
  }
}

export default new CardNavView();
