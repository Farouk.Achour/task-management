class LogoutView {
  element = document.querySelector('.log-out');

  logout() {
    this.element.addEventListener('click', function () {
      localStorage.clear();
      window.location.reload();
    });
  }
}

export default new LogoutView();
