import addTaskView from './addTaskView';

class ProjectView {
  parent = document.querySelector('.navigation-bar__projects');

  renderProject(project, myProject) {
    const html = `
      <div class="navigation-bar__project">
        <div class="project__title__el pointer">
          <div>
            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="16" viewBox="0 0 15 16" fill="none">
              <path d="M5.62507 11.675V4.69379C5.62435 4.56965 5.66061 4.44811 5.72924 4.34466C5.79787 4.24121 5.89574 4.16054 6.0104 4.11293C6.12505 4.06533 6.25128 4.05295 6.373 4.07737C6.49472 4.10178 6.6064 4.1619 6.69382 4.25004L10.1813 7.74379C10.2977 7.86089 10.3631 8.0193 10.3631 8.18441C10.3631 8.34953 10.2977 8.50794 10.1813 8.62504L6.69382 12.1188C6.6064 12.2069 6.49472 12.267 6.373 12.2915C6.25128 12.3159 6.12505 12.3035 6.0104 12.2559C5.89574 12.2083 5.79787 12.1276 5.72924 12.0242C5.66061 11.9207 5.62435 11.7992 5.62507 11.675Z" fill="#98A2B3"/>
            </svg>
          </div>
          <img src="${project.img}" class="project__img" alt="project icon" />
          <p data-name="${project.name}" class="projectName">${project.name}</p>
        </div>
        <div class="project__func__el">
          <div class="pointer">
            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10" fill="none">
              <g clip-path="url(#clip0_103_2804)">
                <path d="M5.00002 1.66667C5.46026 1.66667 5.83335 1.29357 5.83335 0.833333C5.83335 0.373096 5.46026 0 5.00002 0C4.53978 0 4.16669 0.373096 4.16669 0.833333C4.16669 1.29357 4.53978 1.66667 5.00002 1.66667Z" fill="#666666"/>
                <path d="M5.00002 5.8335C5.46026 5.8335 5.83335 5.46041 5.83335 5.00017C5.83335 4.53993 5.46026 4.16684 5.00002 4.16684C4.53978 4.16684 4.16669 4.53993 4.16669 5.00017C4.16669 5.46041 4.53978 5.8335 5.00002 5.8335Z" fill="#666666"/>
                <path d="M5.00002 9.99984C5.46026 9.99984 5.83335 9.62674 5.83335 9.1665C5.83335 8.70626 5.46026 8.33316 5.00002 8.33316C4.53978 8.33316 4.16669 8.70626 4.16669 9.1665C4.16669 9.62674 4.53978 9.99984 5.00002 9.99984Z" fill="#666666"/>
              </g>
              <defs>
                <clipPath id="clip0_103_2804">
                  <rect width="10" height="10" fill="white"/>
                </clipPath>
              </defs>
            </svg>
          </div>
          <div class="pointer">
            <p class="project__func__el__p">+</p>
          </div>
        </div>
      </div>
    `;

    let allDisplayedProjects = new Set();
    document.querySelectorAll('.projectName').forEach(pr => {
      allDisplayedProjects.add(pr.dataset.name);
      return pr.dataset.name;
    });
    if (!allDisplayedProjects.has(project.name))
      this.parent.insertAdjacentHTML('beforeend', html);

    // this.parent.querySelectorAll('.project__func__el__p').forEach(el => {
    //   el.addEventListener('click', function () {
    //     const firstBtnClicked = true;
    //     addTaskView.addTask(myProject, firstBtnClicked);
    //   });
    // });

    this.parent.addEventListener('click', function (e) {
      this.querySelectorAll('.navigation-bar__project').forEach(el => {
        el.classList.remove('navigation-bar__project--selected');
      });

      e.target
        .closest('.navigation-bar__project')
        ?.classList.add('navigation-bar__project--selected');
    });
  }
}

export default new ProjectView();
