import * as model from './../model.js';
import { API_URL } from '../config.js';
import { AJAX } from '../helpers.js';
import addTaskView from './addTaskView.js';
import addStatusView from './addStatusView.js';

class CardListView {
  renderCardList(specificProject, myProjects, specificStatuses) {
    const parent = document.querySelector('.card__container');
    if (specificProject.length === 0) parent.innerHTML = '';

    const html = `
      <div class="card__list__view margin-top">
        <div class="card__list__view__status__container">
        
        </div>
        
        <div class="card__add-status pointer">
          <svg xmlns="http://www.w3.org/2000/svg" width="18" height="19" viewBox="0 0 18 19" fill="none">
            <g clip-path="url(#clip0_1416_1521)">
              <path d="M9 0.5C7.21997 0.5 5.47991 1.02784 3.99987 2.01677C2.51983 3.00571 1.36628 4.41131 0.685088 6.05585C0.00389957 7.70038 -0.17433 9.50998 0.172937 11.2558C0.520204 13.0016 1.37737 14.6053 2.63604 15.864C3.89472 17.1226 5.49836 17.9798 7.24419 18.3271C8.99002 18.6743 10.7996 18.4961 12.4442 17.8149C14.0887 17.1337 15.4943 15.9802 16.4832 14.5001C17.4722 13.0201 18 11.28 18 9.5C17.9974 7.11384 17.0484 4.82616 15.3611 3.13889C13.6738 1.45162 11.3862 0.502581 9 0.5V0.5ZM9 17C7.51664 17 6.0666 16.5601 4.83323 15.736C3.59986 14.9119 2.63856 13.7406 2.07091 12.3701C1.50325 10.9997 1.35473 9.49168 1.64411 8.03682C1.9335 6.58197 2.64781 5.24559 3.6967 4.1967C4.7456 3.14781 6.08197 2.4335 7.53683 2.14411C8.99168 1.85472 10.4997 2.00325 11.8701 2.5709C13.2406 3.13856 14.4119 4.09985 15.236 5.33322C16.0601 6.56659 16.5 8.01664 16.5 9.5C16.4978 11.4885 15.7069 13.3948 14.3009 14.8009C12.8948 16.2069 10.9885 16.9978 9 17ZM12.75 9.5C12.75 9.69891 12.671 9.88968 12.5303 10.0303C12.3897 10.171 12.1989 10.25 12 10.25H9.75V12.5C9.75 12.6989 9.67099 12.8897 9.53033 13.0303C9.38968 13.171 9.19892 13.25 9 13.25C8.80109 13.25 8.61033 13.171 8.46967 13.0303C8.32902 12.8897 8.25 12.6989 8.25 12.5V10.25H6C5.80109 10.25 5.61033 10.171 5.46967 10.0303C5.32902 9.88968 5.25 9.69891 5.25 9.5C5.25 9.30109 5.32902 9.11032 5.46967 8.96967C5.61033 8.82902 5.80109 8.75 6 8.75H8.25V6.5C8.25 6.30109 8.32902 6.11032 8.46967 5.96967C8.61033 5.82902 8.80109 5.75 9 5.75C9.19892 5.75 9.38968 5.82902 9.53033 5.96967C9.67099 6.11032 9.75 6.30109 9.75 6.5V8.75H12C12.1989 8.75 12.3897 8.82902 12.5303 8.96967C12.671 9.11032 12.75 9.30109 12.75 9.5Z" fill="#374957"/>
            </g>
            <defs>
              <clipPath id="clip0_1416_1521">
                <rect width="18" height="18" fill="white" transform="translate(0 0.5)"/>
              </clipPath>
            </defs>
          </svg>
          <p>New State</p>
        </div>
      </div>
    `;

    parent.innerHTML = '';
    parent.insertAdjacentHTML('afterbegin', html);

    //  Add status view
    document
      .querySelector('.card__add-status')
      .addEventListener('click', function (e) {
        e.stopPropagation();
        addStatusView.renderStatusModal();
      });

    let tasksOfProject = 0;

    // Add specific status to the correspondent project
    specificStatuses.map(status => {
      document
        .querySelector('.card__list__view__status__container')
        .insertAdjacentHTML(
          'beforeend',
          `
      <div class="card__list__view__block">
        <div class="card__list__view__block__header">
          <div class="card__list__view__block__header__status">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
              <path d="M12.1921 15.2695L15.9065 10.8121C16.3408 10.2911 15.9702 9.5 15.292 9.5H8.70803C8.02976 9.5 7.65924 10.2911 8.09346 10.8121L11.8079 15.2695C11.9079 15.3895 12.0921 15.3895 12.1921 15.2695Z" fill="#787878"/>
            </svg>
            <div class="card__list__view__block__header__status__text__container">
              <svg xmlns="http://www.w3.org/2000/svg" width="12" height="13" viewBox="0 0 12 13" fill="none">
                <circle cx="6" cy="6.5" r="6" fill="#C5C5C5"/>
              </svg>
              <p class="card__list__view__block__header__status__text">
                ${status.attributes.status}
              </p>
              <p class="card__list__view__block__header__status__list-count">
                ${status.attributes?.tasks?.data?.length}
              </p>
            </div>
            <div class="card__view__add-task pointer">
              <p>+ New task</p>
            </div>
          </div>
          <div class="card__list__view__block__header__details">
            <p>Assignee</p>
            <p>Due Data</p>
            <p>Priority</p>
            <p>More</p>
          </div>
          
        </div>
        <div class="card__list__view__tasks" data-status="${status.attributes.status}">
              
        </div>
      </div>
      `
        );

      // document.querySelector('.card__list__view__tasks').innerHTML = '';

      tasksOfProject = status.attributes?.tasks?.data.length + tasksOfProject;

      status.attributes?.tasks?.data.map(async task => {
        const taskStatus = await AJAX(
          `${API_URL}/tasks/${task.id}/?populate=*`
        );

        console.log(task, document.querySelector('.card__list__view__tasks'));

        console.log();
        document
          .querySelectorAll('.card__list__view__tasks')
          .forEach(stateField => {
            console.log(stateField.dataset.status);
            console.log(
              stateField.dataset.status ===
                taskStatus.data.attributes.statuses.data[0].attributes.status
            );
            if (
              stateField.dataset.status ===
              taskStatus.data.attributes.statuses.data[0].attributes.status
            ) {
              console.log(stateField);
              stateField.insertAdjacentHTML(
                'afterbegin',
                `<div class="card__list__view__task" draggable="true" data-id="${task.id}">
              <div class="card__list__view__task__name__container">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
                  <path d="M14 18.5C14 19.0523 14.4477 19.5 15 19.5C15.5523 19.5 16 19.0523 16 18.5C16 17.9477 15.5523 17.5 15 17.5C14.4477 17.5 14 17.9477 14 18.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M8 18.5C8 19.0523 8.44772 19.5 9 19.5C9.55228 19.5 10 19.0523 10 18.5C10 17.9477 9.55228 17.5 9 17.5C8.44772 17.5 8 17.9477 8 18.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M14 12.5C14 13.0523 14.4477 13.5 15 13.5C15.5523 13.5 16 13.0523 16 12.5C16 11.9477 15.5523 11.5 15 11.5C14.4477 11.5 14 11.9477 14 12.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M8 12.5C8 13.0523 8.44772 13.5 9 13.5C9.55228 13.5 10 13.0523 10 12.5C10 11.9477 9.55228 11.5 9 11.5C8.44772 11.5 8 11.9477 8 12.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M14 6.5C14 7.05228 14.4477 7.5 15 7.5C15.5523 7.5 16 7.05228 16 6.5C16 5.94772 15.5523 5.5 15 5.5C14.4477 5.5 14 5.94772 14 6.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                  <path d="M8 6.5C8 7.05228 8.44772 7.5 9 7.5C9.55228 7.5 10 7.05228 10 6.5C10 5.94772 9.55228 5.5 9 5.5C8.44772 5.5 8 5.94772 8 6.5Z" stroke="#BABABA" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="25" viewBox="0 0 24 25" fill="none">
                  <path d="M14.7695 12.3079L10.3121 8.59346C9.79109 8.15924 9 8.52976 9 9.20803V15.792C9 16.4702 9.79109 16.8408 10.3121 16.4065L14.7695 12.6921C14.8895 12.5921 14.8895 12.4079 14.7695 12.3079Z" fill="#AFAFAF"/>
                </svg>
                <div class="card__list__view__task__div">
                  <svg xmlns="http://www.w3.org/2000/svg" width="10" height="11" viewBox="0 0 10 11" fill="none">
                    <circle cx="5" cy="5.5" r="5" fill="#C5C5C5"/>
                  </svg>
                  <p>${task.attributes.title}</p>
                </div>
              </div>
              <div class="card__list__view__task__details"></div>
            </div>`
              );
            }
          });

        console.log(task.id);

        console.log(
          taskStatus.data.attributes.statuses.data[0].attributes.status
        );

        ///////////////////////////////////////////////////////////
        // Drag && Drop To change completly
        // To Do: Drag & Drop Tasks between multiple statuses

        console.log(
          tasksOfProject,
          document.querySelectorAll('.card__list__view__task').length
        );

        if (
          tasksOfProject ===
          document.querySelectorAll('.card__list__view__task').length
        ) {
          const draggables = document.querySelectorAll(
            '.card__list__view__task'
          );
          const containers = document.querySelectorAll(
            '.card__list__view__tasks'
          );

          const getDragAfterElement = function (container, y) {
            const draggableElements = [
              ...container.querySelectorAll(
                '.card__list__view__task:not(.dragging)'
              ),
            ];

            return draggableElements.reduce(
              (closest, child) => {
                const box = child.getBoundingClientRect();
                const offset = y - box.top - box.height / 2;
                if (offset < 0 && offset > closest.offset) {
                  return { offset: offset, element: child };
                } else {
                  return closest;
                }
              },
              { offset: Number.NEGATIVE_INFINITY }
            ).element;
          };

          draggables.forEach(draggable => {
            draggable.addEventListener('dragstart', function () {
              draggable.classList.add('dragging');
            });

            draggable.addEventListener('dragend', async function () {
              draggable.classList.remove('dragging');

              // debugger;

              document
                .querySelectorAll('.card__list__view__tasks')
                .forEach(async statusList => {
                  console.log(statusList);
                  // statusList.innerHTML = '';
                  const currentPosition = statusList.querySelectorAll(
                    '.card__list__view__task'
                  );
                  console.log(currentPosition);

                  const newTasksOrder = {
                    data: {
                      tasks: [],
                    },
                  };
                  currentPosition.forEach(draggable => {
                    newTasksOrder.data.tasks.unshift({
                      id: parseInt(draggable.dataset.id),
                    });
                  });

                  // console.log(myProject);
                  const stateDrop = specificStatuses.find(
                    el => el.attributes.status === statusList.dataset.status
                  );

                  await model.updateRelations(
                    `${API_URL}/statuses/${stateDrop.id}`,
                    newTasksOrder
                  );
                });
              document.querySelector('body').click();
            });
          });

          containers.forEach(container => {
            container.addEventListener('dragover', function (e) {
              e.preventDefault();
              const afterElement = getDragAfterElement(container, e.clientY);
              const draggable = document.querySelector('.dragging');
              if (afterElement == null) {
                container.appendChild(draggable);
              } else {
                container.insertBefore(draggable, afterElement);
              }
            });
          });
        }

        ///////////////////////////////////
      });
    });

    parent.querySelectorAll('.card__view__add-task').forEach(el => {
      el.addEventListener('click', function () {
        const btnClicked = true;
        addTaskView.addTask(myProjects, btnClicked);
        this.click();
      });
    });
  }
}

export default new CardListView();
