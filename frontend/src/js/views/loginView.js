class LoginView {
  _form = document.querySelector('.form--login');

  _email = document.querySelector('.input__email');
  _password = document.querySelector('.input__password');

  user = {};

  toLogin() {
    window.location.replace('http://localhost:1234/login.html');
    localStorage.clear();
  }

  async addUser() {
    const handler = function (e) {
      e.preventDefault();
      this.user = {
        identifier: this._email.value,
        password: this._password.value,
      };
    };

    this._form.addEventListener('submit', handler.bind(this));

    const { data } = await new Promise(handler => {
      this._form.addEventListener('submit', handler.bind(this));
    });
    return data;
  }
}

export default new LoginView();
