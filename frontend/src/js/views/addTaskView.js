import { API_URL } from './../config.js';
import { AJAX } from './../helpers.js';
import * as model from './../model.js';

class AddTaskView {
  btnAddTask = document.querySelector('.btn--newtask');

  async saveTask(title, description, priority, attachement, myStatus) {
    try {
      const fileReader = new FileReader();
      let attachementEl;
      if (attachement) {
        fileReader.readAsDataURL(attachement);
        fileReader.addEventListener('load', function () {
          attachementEl = this.result;
        });
      } else attachementEl = '';

      // Task is added to project so I have to add it to correspond it status instead

      const filtredTasks = await model.findMyData(myStatus, 'tasks');

      const task = {
        data: {
          title: title,
          description: description,
          priority: priority,
          attachement: attachementEl,
        },
      };

      const currentTask = await AJAX(`${API_URL}/tasks`, task);

      const tasks = {
        data: {
          tasks: [...filtredTasks, currentTask.data],
        },
      };

      await model.updateRelations(`${API_URL}/statuses/${myStatus.id}`, tasks);

      document.querySelector('body').click();
    } catch (err) {
      return `${err.message}`;
    }
  }

  addTask(myProj, btnAddTaskHandler) {
    let proj;
    let projectStatuses;
    let saveTask = this.saveTask;
    let currentStatus;
    console.log(document.querySelectorAll('.card__view__add-task'));
    document.querySelectorAll('.card__view__add-task').forEach(pr => {
      console.log(pr);
      pr.addEventListener('click', function () {
        console.log(pr);
        currentStatus = this.closest('.card__list__view__block__header__status')
          .querySelector('.card__list__view__block__header__status__text')
          .textContent.trim();
        setTimeout(async () => {
          proj = myProj.filter(
            proj =>
              proj.attributes.name ===
              document
                .querySelector('.navigation-bar__project--selected')
                ?.querySelector('.project__title__el')
                ?.querySelector('p').textContent
          );

          projectStatuses = await model.findMyData(proj[0], 'statuses');

          // const specificStatus = projectStatuses.filter(
          //   state => state.attributes.status === currentStatus
          // )[0];
          // console.log(specificStatus);
        }, 300);

        setTimeout(() => {
          let statusColor = '#FFA948';
          let priorityColor = '#FFB700';
          let priorityText = 'High';

          console.log(
            this.closest('.card__list__view__block__header__status')
              .querySelector('.card__list__view__block__header__status__text')
              .textContent.trim()
          );

          const html = `
            <div class="add-task-window">
              <div class="modal task--modal">
                <div class="task__header">
                  <p class="task__location">Project</p>
                  <div class="task__close">
                    <span class="task__date">Created 27/11/23</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27" fill="none">
                      <path d="M13.0975 13.2023H13.8964M13.0975 14.0012H13.8964M5.6412 13.2023H6.44009M5.6412 14.0012H6.44009M20.5538 13.2023H21.3527M20.5538 14.0012H21.3527M14.5624 13.6018C14.5624 14.1901 14.0855 14.667 13.4972 14.667C12.9089 14.667 12.432 14.1901 12.432 13.6018C12.432 13.0135 12.9089 12.5366 13.4972 12.5366C14.0855 12.5366 14.5624 13.0135 14.5624 13.6018ZM7.1061 13.6018C7.1061 14.1901 6.6292 14.667 6.04091 14.667C5.45262 14.667 4.97572 14.1901 4.97572 13.6018C4.97572 13.0135 5.45262 12.5366 6.04091 12.5366C6.6292 12.5366 7.1061 13.0135 7.1061 13.6018ZM22.0187 13.6018C22.0187 14.1901 21.5418 14.667 20.9536 14.667C20.3653 14.667 19.8884 14.1901 19.8884 13.6018C19.8884 13.0135 20.3653 12.5366 20.9536 12.5366C21.5418 12.5366 22.0187 13.0135 22.0187 13.6018Z" stroke="#374957" stroke-width="2.13038" stroke-linecap="round"/>
                    </svg>
                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="22" viewBox="0 0 21 22" fill="none" class="task__close-icon pointer">
                      <g clip-path="url(#clip0_2_4937)">
                        <path d="M12.9999 13.4684L10.4701 10.9386M10.4701 10.9386L7.94026 8.40879M10.4701 10.9386L7.94026 13.4684M10.4701 10.9386L12.9999 8.40879M1.19406 10.9386C1.19406 16.0616 5.34708 20.2146 10.4701 20.2146C15.5931 20.2146 19.7461 16.0616 19.7461 10.9386C19.7461 5.81561 15.5931 1.6626 10.4701 1.6626C5.34708 1.6626 1.19406 5.81561 1.19406 10.9386Z" stroke="#374957" stroke-width="2.13038" stroke-linecap="round"/>
                      </g>
                      <defs>
                        <clipPath id="clip0_2_4937">
                          <rect width="20.2386" height="20.2386" fill="white" transform="translate(0.350815 0.81958)"/>
                        </clipPath>
                      </defs>
                    </svg>
                  </div>
                </div>
                <form class="task__modal__content">
                  <div class="task__modal__main">
                    <div class="task__modal__main__header">
                      <div class="task__modal__status pointer">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13" fill="none">
                          <circle cx="6.56458" cy="6.20874" r="6" fill="${statusColor}"/>
                        </svg>
                        <p class="task__modal__status__text">${currentStatus}</p>
                        <svg xmlns="http://www.w3.org/2000/svg" width="9" height="7" viewBox="0 0 9 7" fill="none">
                          <path d="M4.75663 5.97827L8.47112 1.52089C8.90534 0.999826 8.53481 0.20874 7.85654 0.20874L1.27261 0.20874C0.594338 0.20874 0.223813 0.999825 0.658031 1.52089L4.37252 5.97827C4.47247 6.09821 4.65668 6.09821 4.75663 5.97827Z" fill="${statusColor}"/>
                        </svg>
                      </div>
                      <img src="img/addAvatar.png" />
                      <div class="task__modal__priority">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="19" viewBox="0 0 17 19" fill="none">
                        <path d="M1.56458 1.20874V11.6373H14.2994C14.8345 11.6373 15.102 11.6373 15.1645 11.4792C15.227 11.3211 15.0317 11.1382 14.6413 10.7724L10.0429 6.46495C9.89682 6.32813 9.82379 6.25972 9.82379 6.17303C9.82379 6.08633 9.89682 6.01792 10.0429 5.8811L14.6413 1.57365C15.0317 1.20786 15.227 1.02497 15.1645 0.866853C15.102 0.70874 14.8345 0.70874 14.2994 0.70874H2.06458C1.82887 0.70874 1.71102 0.70874 1.6378 0.781964C1.56458 0.855187 1.56458 0.973038 1.56458 1.20874Z" fill="${priorityColor}"/>
                          <path d="M1.56458 11.6373V1.20874C1.56458 0.973038 1.56458 0.855187 1.6378 0.781964C1.71102 0.70874 1.82887 0.70874 2.06458 0.70874H14.2994C14.8345 0.70874 15.102 0.70874 15.1645 0.866853C15.227 1.02497 15.0317 1.20786 14.6413 1.57365L10.0429 5.8811C9.89682 6.01792 9.82379 6.08633 9.82379 6.17303C9.82379 6.25972 9.89682 6.32813 10.0429 6.46495L14.6413 10.7724C15.0317 11.1382 15.227 11.3211 15.1645 11.4792C15.102 11.6373 14.8345 11.6373 14.2994 11.6373H1.56458ZM1.56458 11.6373V17.7087" stroke="${priorityColor}" stroke-linecap="round"/>
                        </svg>
                        <p class="task__modal__priority__text">${priorityText}</p>
                      </div>
                      <img src="img/taskTime.png">
                    </div>

                    <div class="task__modal__main__body">
                      <div class="task__modal__main__body__header">
                        <svg xmlns="http://www.w3.org/2000/svg" width="2" height="25" viewBox="0 0 2 25" fill="none">
                          <path d="M0.564575 0.637451V24.6042" stroke="#CCD2E3" stroke-width="1.06519"/>
                        </svg>
                        <input type="text" placeholder="Add Task's name" class="task__modal__main__body__header__task-name" >
                      </div>
                      <div class="task__modal__main__body__header__task-description__container">
                        <svg xmlns="http://www.w3.org/2000/svg" width="19" height="18" viewBox="0 0 19 18" fill="none">
                          <path d="M10.9941 13.3913C10.9941 13.7985 11.3242 14.1285 11.7314 14.1285C12.1386 14.1285 12.4686 13.7985 12.4686 13.3913C12.4686 12.9841 12.1386 12.6541 11.7314 12.6541C11.3242 12.6541 10.9941 12.9841 10.9941 13.3913Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                          <path d="M6.57068 13.3913C6.57068 13.7985 6.90075 14.1285 7.30792 14.1285C7.71509 14.1285 8.04517 13.7985 8.04517 13.3913C8.04517 12.9841 7.71509 12.6541 7.30792 12.6541C6.90075 12.6541 6.57068 12.9841 6.57068 13.3913Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                          <path d="M10.9941 8.96783C10.9941 9.375 11.3242 9.70508 11.7314 9.70508C12.1386 9.70508 12.4686 9.375 12.4686 8.96783C12.4686 8.56067 12.1386 8.23059 11.7314 8.23059C11.3242 8.23059 10.9941 8.56067 10.9941 8.96783Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                          <path d="M6.57068 8.96783C6.57068 9.375 6.90075 9.70508 7.30792 9.70508C7.71509 9.70508 8.04517 9.375 8.04517 8.96783C8.04517 8.56067 7.71509 8.23059 7.30792 8.23059C6.90075 8.23059 6.57068 8.56067 6.57068 8.96783Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                          <path d="M10.9941 4.54437C10.9941 4.95154 11.3242 5.28162 11.7314 5.28162C12.1386 5.28162 12.4686 4.95154 12.4686 4.54437C12.4686 4.1372 12.1386 3.80713 11.7314 3.80713C11.3242 3.80713 10.9941 4.1372 10.9941 4.54437Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                          <path d="M6.57068 4.54437C6.57068 4.95154 6.90075 5.28162 7.30792 5.28162C7.71509 5.28162 8.04517 4.95154 8.04517 4.54437C8.04517 4.1372 7.71509 3.80713 7.30792 3.80713C6.90075 3.80713 6.57068 4.1372 6.57068 4.54437Z" stroke="#ABB2C8" stroke-width="1.47449" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <textarea class="task__modal__main__body__header__task-description" placeholder="Add description"></textarea>
                      </div>
                      <img src="img/taskAddTags.png"/>
                      <div class="task__modal__attachement">
                        <p class="task__modal__attachement__text">Attachements</p>
                        <label class="task__modal__attachement__input"><svg xmlns="http://www.w3.org/2000/svg" width="27" height="26" viewBox="0 0 27 26" fill="none">
                        <path d="M15.4772 11.0322L11.2165 15.293" stroke="#222222" stroke-width="1.06519" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M17.6076 14.2279L19.738 12.0976C21.2087 10.6268 21.2087 8.24233 19.738 6.77162V6.77162C18.2672 5.3009 15.8827 5.3009 14.412 6.77161L12.2816 8.90199M9.08608 12.0976L6.9557 14.2279C5.48498 15.6987 5.48498 18.0832 6.9557 19.5539V19.5539C8.42642 21.0246 10.8109 21.0246 12.2816 19.5539L14.412 17.4235" stroke="#222222" stroke-width="1.06519" stroke-linecap="round"/>
                      </svg> <spna>Drag and drop files to attach or <span>Browse</span></spna><input type="file"></label>
                      </div>
                    </div>
                  </div>
                  <div class="task__modal__activity">
                    <div class="task__modal__activity__header">
                      <p class="task__modal__activity__heading">Activity</p>
                      <div class="task__modal__activity__content">
                        <svg xmlns="http://www.w3.org/2000/svg" width="5" height="9" viewBox="0 0 5 9" fill="none">
                          <path d="M0.585429 7.78248V1.28413C0.584757 1.16857 0.618512 1.05544 0.682391 0.959143C0.74627 0.862849 0.837378 0.787759 0.9441 0.743448C1.05082 0.699136 1.16832 0.687611 1.28162 0.71034C1.39492 0.73307 1.49888 0.789025 1.58025 0.871071L4.82653 4.12316C4.93488 4.23216 4.9957 4.37961 4.9957 4.5333C4.9957 4.687 4.93488 4.83445 4.82653 4.94345L1.58025 8.19553C1.49888 8.27758 1.39492 8.33353 1.28162 8.35626C1.16832 8.37899 1.05082 8.36747 0.9441 8.32316C0.837378 8.27884 0.74627 8.20376 0.682391 8.10746C0.618512 8.01117 0.584757 7.89803 0.585429 7.78248Z" fill="#FFA948"/>
                        </svg>
                        <p class="task__modal__activity__content__text">${
                          JSON.parse(localStorage.getItem('data')).username
                        } create this task</p>
                      </div> 
                    </div>
                    <div class="task__modal__activity__form">
                      <input class="task__modal__activity__form__input" placeholder="Comment" />
                      <div class="btn--comment pointer">
                        <p>Envoyer</p>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="overlay overlay--task"></div>
            </div>
          `;

          if (btnAddTaskHandler && document.querySelector('.modal') === null) {
            document
              .querySelector('body')
              .insertAdjacentHTML('afterbegin', html);
          }

          document
            .querySelector('.task__modal__status')
            ?.addEventListener('click', function (e) {
              e.stopPropagation();

              const statusModal = `
                  <div class="status__window">
                    <div class="status__modal">
                      
                    </div>
                    <div class="status__overlay"></div>
                  </div>
                `;

              if (document.querySelector('.status__window') === null) {
                this.insertAdjacentHTML('afterbegin', statusModal);

                projectStatuses.map(status => {
                  this.querySelector('.status__modal').insertAdjacentHTML(
                    'afterbegin',
                    `
                    <p class="status__modal__el">
                      ${status.attributes.status}
                    </p>
                    `
                  );
                });

                this.querySelector('.status__modal').addEventListener(
                  'click',
                  function (e) {
                    e.stopPropagation();
                    console.log(e.target.textContent.trim());
                    this.closest('.task__modal__status').querySelector(
                      '.task__modal__status__text'
                    ).textContent = e.target.textContent.trim();
                    this.closest('.status__window').remove();
                  }
                );
              }

              document
                .querySelector('.status__overlay')
                .addEventListener('click', function (e) {
                  e.stopPropagation();
                  this.closest('.status__window').remove();
                });

              // fetch for my statuses then render them iside the model
            });

          const closeModal = function () {
            document
              .querySelector('form')
              .addEventListener('submit', function (e) {
                e.preventDefault();
              });

            if (document.querySelector('.add-task-window') !== null) {
              document.addEventListener('keyup', function (e) {
                const specificStatus = projectStatuses.filter(
                  state =>
                    state.attributes.status ===
                    this.querySelector(
                      '.task__modal__status__text'
                    )?.textContent.trim()
                )[0];

                const requirement =
                  this.querySelector(
                    '.task__modal__main__body__header__task-name'
                  )?.value &&
                  this.querySelector(
                    '.task__modal__main__body__header__task-description'
                  )?.value
                    ? true
                    : false;

                if (e.code === 'Enter' && requirement) {
                  // Here I must upload data to Strapi
                  saveTask(
                    this.querySelector(
                      '.task__modal__main__body__header__task-name'
                    ).value,
                    this.querySelector(
                      '.task__modal__main__body__header__task-description'
                    ).value,
                    this.querySelector(
                      '.task__modal__priority__text'
                    ).textContent.toLowerCase(),
                    this.querySelector(
                      '.task__modal__attachement__input'
                    ).querySelector('input').files[0],
                    specificStatus,
                    this.querySelector('.task__modal__status__text').textContent
                  );

                  this.querySelector('.add-task-window').remove();
                  // window.location.reload();
                }
              });
            }

            document
              .querySelector('.overlay')
              ?.addEventListener('click', function () {
                // this.saveTask();
                this.closest('.add-task-window').remove();
                // window.location.reload();
              }); //list

            document
              .querySelector('.task__close-icon')
              ?.addEventListener('click', function () {
                // this.saveTask();
                this.closest('.add-task-window').remove();
                // window.location.reload();
              });
          };
          closeModal();
        }, 200);
      });
    });
  }
}

export default new AddTaskView();
