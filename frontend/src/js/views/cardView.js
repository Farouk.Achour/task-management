import * as model from './../model.js';
import cardListView from './cardListView.js';
import { AJAX } from '../helpers.js';
import { API_URL } from '../config.js';
import cardBoardView from './cardBoardView.js';

class CardView {
  async renderCard(myProjects) {
    console.log(document.querySelector('.navigation-bar__project--selected'));
    if (document.querySelector('.navigation-bar__project--selected') === null)
      return;

    const project = myProjects.filter(
      proj =>
        proj.attributes.name ===
        document
          .querySelector('.navigation-bar__project--selected')
          .querySelector('.project__title__el')
          .querySelector('p').textContent
    );

    console.log(project);

    const specificProject = await AJAX(
      `${API_URL}/projects/${project[0].id}/?populate=*`
    );
    console.log(specificProject);

    const specificStatuses = specificProject.data.attributes.statuses.data.map(
      status => status.id
    );

    let statuses = [];
    specificStatuses.map(async stateId => {
      const data = await AJAX(`${API_URL}/statuses/${stateId}/?populate=*`);
      console.log(data.data);
      statuses = [...statuses, data.data];
    });

    setTimeout(() => {
      console.log(statuses);

      const view = document
        .querySelector('.card__nav__link--active')
        .querySelector('span').textContent;

      if (view === 'List') {
        console.log('List View');
        cardListView.renderCardList(specificProject, myProjects, statuses);
      }

      if (view === 'Board') {
        console.log('Board View');
        cardBoardView.renderCardBoard(specificProject, myProjects, statuses);
      }
    }, 100);
  }
}

export default new CardView();
