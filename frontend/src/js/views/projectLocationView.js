class ProjectLocationView {
  parent = document.querySelector('.header__location__title');

  renderProjectLocation() {
    console.log(document.querySelector('.navigation-bar__project--selected'));

    window.addEventListener('click', function () {
      const project = document
        .querySelector('.navigation-bar__project--selected')
        ?.querySelector('.project__title__el')
        ?.querySelector('p')?.textContent;
      console.log(project);

      if (project) {
        document
          .querySelector('.header__location')
          .querySelector(
            '.header__location__title'
          ).textContent = `Project > ${project}`;
      }
    });
  }
}

export default new ProjectLocationView();
