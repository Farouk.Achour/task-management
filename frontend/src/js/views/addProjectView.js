import { API_URL } from './../config.js';
import { AJAX } from './../helpers.js';
import * as model from './../model.js';

class AddProjectView {
  btn = document.querySelector('.project__btn');

  async getProjectInput(currentUser) {
    const html = `
      <div class="add-project-window">
        <div class="modal">
          <h1 class="modal__project__heading">Add a new Project</h1>
          <form class="modal__project__form">
            <div>
              <p class="modal__project__label">Project's name </p>
              <input class="modal__project__input project__input--name" type="text" placeholder="Project's name">
            </div>
            <div>
              <p class="modal__project__label">Project's image url </p>
              <input class="modal__project__input project__input--img" type="text" placeholder="Project's image">
            </div>
            <button class="pointer btn--add-project">Submit</button>
          </form>
        </div>
        <div class="overlay"></div>
      </div>
    `;

    const btn = this.btn;

    const registerProject = async function () {
      btn.addEventListener('click', function (e) {
        e.preventDefault();
        this.closest('body').insertAdjacentHTML('afterbegin', html);

        this.closest('body')
          .querySelector('form')
          .addEventListener('click', function (e) {
            e.preventDefault();
            const name = this.closest('.modal').querySelector(
              '.project__input--name'
            ).value;
            if (name === '') return;
            const imgURL = this.closest('.modal').querySelector(
              '.project__input--img'
            ).value;
            if (imgURL === '') return;

            const project = {
              data: {
                name: name,
                img: imgURL,
              },
            };

            const uploadProject = async function (project) {
              try {
                const filteredProjects = await model.findMyData(
                  currentUser,
                  'projects'
                );

                const projectEl = await AJAX(`${API_URL}/projects`, project);

                // I added filtered Projects
                const projects = {
                  projects: [...filteredProjects, projectEl.data],
                };

                console.log(projects);

                await model.updateRelations(
                  `${API_URL}/users/${currentUser.id}`,
                  projects
                );

                // Remove window
                document.querySelector('.add-project-window').remove();

                window.location.reload();
              } catch (err) {
                return `${err.message}`;
              }
            };
            uploadProject(project);
          });

        this.closest('body')
          .querySelector('.overlay')
          .addEventListener('click', function () {
            this.closest('.add-project-window').remove();
          });
      });
    };
    registerProject();
  }
}

export default new AddProjectView();
