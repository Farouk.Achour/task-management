class SignupView {
  _form = document.querySelector('.form--signup');

  _fullName = document.querySelector('.input__name');
  _email = document.querySelector('.input__email');
  _password = document.querySelector('.input__password');
  _phone = document.querySelector('.input__phone');

  user = {};

  toSignup() {
    console.log(1);
    window.location.replace('http://localhost:1234/signup.html');
    localStorage.clear();
  }

  async addUser() {
    const handler = function (e) {
      e.preventDefault();
      this.user = {
        username: this._fullName.value,
        email: this._email.value,
        password: this._password.value,
        phone: this._phone.value,
      };
    };

    this._form.addEventListener('submit', handler.bind(this));

    const { data } = await new Promise(handler => {
      this._form.addEventListener('submit', handler.bind(this));
    });
    return data;
  }
}

export default new SignupView();
