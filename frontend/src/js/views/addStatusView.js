import * as model from './../model.js';
import { AJAX } from '../helpers.js';
import { API_URL } from '../config.js';
import projectView from './projectView.js';

class CardView {
  renderStatusModal() {
    const parent = document.querySelector('.card__add-status');

    // status design
    const html = `
      <div class="add-status__modal">
        <div>
          <input class="add-status__modal__input" placeholder="Status name...">
        </div>
        <div class="add-status__modal__submit pointer">
          <svg xmlns="http://www.w3.org/2000/svg" width="17" height="18" viewBox="0 0 17 18" fill="none">
            <path d="M10.625 6.16667H6.09167C5.69496 6.16667 5.49661 6.16667 5.34509 6.08946C5.21181 6.02155 5.10345 5.91319 5.03554 5.77991C4.95833 5.62839 4.95833 5.43004 4.95833 5.03333V2.625M12.0417 15.375V10.8417C12.0417 10.445 12.0417 10.2466 11.9645 10.0951C11.8966 9.96181 11.7882 9.85345 11.6549 9.78554C11.5034 9.70833 11.305 9.70833 10.9083 9.70833H6.09167C5.69496 9.70833 5.49661 9.70833 5.34509 9.78554C5.21181 9.85345 5.10345 9.96181 5.03554 10.0951C4.95833 10.2466 4.95833 10.445 4.95833 10.8417V15.375M14.875 7.10555V11.975C14.875 13.1651 14.875 13.7602 14.6434 14.2147C14.4397 14.6146 14.1146 14.9397 13.7147 15.1434C13.2602 15.375 12.6651 15.375 11.475 15.375H5.525C4.33489 15.375 3.73983 15.375 3.28527 15.1434C2.88543 14.9397 2.56034 14.6146 2.35661 14.2147C2.125 13.7602 2.125 13.1651 2.125 11.975V6.025C2.125 4.83489 2.125 4.23983 2.35661 3.78527C2.56034 3.38543 2.88543 3.06034 3.28527 2.85661C3.73983 2.625 4.33489 2.625 5.525 2.625H10.3944C10.741 2.625 10.9142 2.625 11.0772 2.66414C11.2218 2.69885 11.36 2.75609 11.4867 2.83376C11.6297 2.92137 11.7522 3.04388 11.9972 3.28889L14.2111 5.50278C14.4561 5.74779 14.5786 5.8703 14.6662 6.01326C14.7439 6.14002 14.8012 6.2782 14.8359 6.42275C14.875 6.5858 14.875 6.75905 14.875 7.10555Z" stroke="white" stroke-linecap="round" stroke-linejoin="round"/>
          </svg>
          <p>Save</p>
        </div>
      </div>
    `;
    console.log(parent);
    if (document.querySelector('.add-status__modal'))
      document.querySelector('.add-status__modal').remove();

    let statusModalIsDisplayed;
    parent.insertAdjacentHTML('afterend', html);
    if (document.querySelector('.add-status__modal')) {
      document
        .querySelector('.add-status__modal')
        .addEventListener('click', function (e) {
          e.stopPropagation();
        });
      statusModalIsDisplayed = true;
    }
    if (statusModalIsDisplayed) parent.style.background = '#bde0f4';

    // add status to strapi using POST method AJAX
    document
      .querySelector('.add-status__modal__submit')
      .addEventListener('click', async function (e) {
        e.stopPropagation();
        console.log(
          this.closest('.add-status__modal').querySelector(
            '.add-status__modal__input'
          ).value
        );

        console.log(
          this.closest('body')
            .querySelector('.navigation-bar__project--selected')
            .querySelector('.project__title__el')
            .querySelector('p').textContent
        );

        const myProj = await model.findMyData(
          JSON.parse(localStorage.getItem('data')),
          'projects'
        );
        console.log(myProj);
        myProj.map(project =>
          projectView.renderProject(project.attributes, myProj)
        );

        const proj = myProj.filter(
          proj =>
            proj.attributes.name ===
            document
              .querySelector('.navigation-bar__project--selected')
              .querySelector('.project__title__el')
              .querySelector('p').textContent
        )[0];
        console.log(proj);

        const filtredStatuses = await model.findMyData(proj, 'statuses');

        console.log(filtredStatuses);

        const status = {
          data: {
            status: this.closest('.add-status__modal').querySelector(
              '.add-status__modal__input'
            ).value,
          },
        };

        const currentStatus = await AJAX(`${API_URL}/statuses`, status);

        const statuses = {
          data: {
            statuses: [...filtredStatuses, currentStatus.data],
          },
        };

        await model.updateRelations(`${API_URL}/projects/${proj.id}`, statuses);

        document.querySelector('body').click();

        this.closest('.add-status__modal').remove();
      });
  }
}

export default new CardView();
