class ErrorView {
  parent = document.querySelector('.btn');

  renderError(err) {
    const html = `
      <div class="error">
        <p>${err}</p>
      </div>
    `;

    console.log(document.querySelector('.error'));
    if (document.querySelector('.error') !== null) return;
    setTimeout(() => {
      this.parent.insertAdjacentHTML('beforebegin', html);
    }, 300);
    setTimeout(() => {
      document.querySelector('.error').remove();
    }, 3000);
  }
}

export default new ErrorView();
