import signupView from './views/signupView.js';
import loginView from './views/loginView.js';
import errorView from './views/errorView.js';
import dashboardView from './views/dashboardView.js';
import addProjectView from './views/addProjectView.js';
import projectView from './views/projectView.js';
import projectLocationView from './views/projectLocationView.js';
import addTaskView from './views/addTaskView.js';
import cardNavView from './views/cardNavView.js';
import cardView from './views/cardView.js';
import logoutView from './views/logoutView.js';
import * as model from './model.js';
import addProjectView from './views/addProjectView.js';

if (module.hot) {
  module.hot.accept();
}

// Register - login - store users in Strapi - enter dashboard
const form = document.querySelector('.form');
const toDashboard = function () {
  setTimeout(() => {
    if (localStorage.getItem('jwt') !== null) {
      dashboardView.goToDashboard();
    }
  }, 200);
};
const init = async function () {
  if (window.location.href === 'http://localhost:1234/login.html') {
    await loginView.addUser();
    const person = loginView.user;
    model.state.push(person);
    const err = await model.findUser(model.state[model.state.length - 1]);
    errorView.renderError(err);
    toDashboard();
  } else if (
    window.location.href === 'http://localhost:1234/signup.html' ||
    window.location.href === 'http://localhost:1234'
  ) {
    await signupView.addUser();
    const person = signupView.user;
    model.state.push(person);
    const err = await model.uploadUser(model.state[model.state.length - 1]);
    errorView.renderError(err);
    toDashboard();
  }
};
form.addEventListener('submit', function (e) {
  e.preventDefault();
  init();
});

// logout
const logout = function () {
  if (window.location.href === 'http://localhost:1234/dashboard.html')
    logoutView.logout();
};
logout();

// Redirection
const redirection = function () {
  if (localStorage.getItem('jwt') !== null) {
    if (window.location.href === 'http://localhost:1234/dashboard.html') return;
    dashboardView.goToDashboard();
  }
  if (
    (window.location.href === 'http://localhost:1234/dashboard.html' &&
      localStorage.getItem('jwt') === null) ||
    (window.location.href === 'http://localhost:1234/dashboard.html' &&
      localStorage.getItem('jwt') === 'undefined')
  )
    signupView.toSignup();
};
redirection();

// Add Project and render it
const mainLogic = async function () {
  if (window.location.href === 'http://localhost:1234/dashboard.html') {
    // get the object of the current user
    const me = JSON.parse(localStorage.getItem('data'));

    // Get users from Strapi
    const state = await model.findUsers();
    model.state.push(state);

    // Register project in Strapi using a modal then link it to the current user

    addProjectView.getProjectInput(me);

    // Render projects : To Do
    const myProj = await model.findMyData(me, 'projects');
    console.log(myProj);
    myProj.map(project =>
      projectView.renderProject(project.attributes, myProj)
    );

    projectLocationView.renderProjectLocation();

    // Add Task to Strapi
    const addTask = async function (myProject) {
      console.log(myProject);
      addTaskView.addTask(myProject);
    };
    await addTask(myProj);

    // To Do: Render list & board View with drag & drop
    cardNavView.renderCardNav();
    window.addEventListener('click', function () {
      cardView.renderCard(myProj);
    });
  }
};
mainLogic();
